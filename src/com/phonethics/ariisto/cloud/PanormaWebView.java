package com.phonethics.ariisto.cloud;




import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class PanormaWebView extends Activity {
	
	WebView 	web;
	ImageView 	image_tap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panorma_web_view);
        
        setTitle("Ariisto Sapphire");
		web	=	(WebView)findViewById(R.id.webView);
		
		web.getSettings().setJavaScriptEnabled(true);
		//web.getSettings().setPluginState(PluginState.ON);
		web.setWebViewClient(new WebViewClient());
		web.loadUrl("file:///android_asset/index.html");
		addListenerOnButton();

    }
    
    public void addListenerOnButton() {

		image_tap = (ImageView) findViewById(R.id.imageButton);

		image_tap.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				Intent mainactivity = new Intent(getApplicationContext(),  Projects.class);
				startActivity(mainactivity);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				finish();
			}
		});

	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_panorma_web_view, menu);
        return false;
    }
}
