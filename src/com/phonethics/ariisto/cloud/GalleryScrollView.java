package com.phonethics.ariisto.cloud;



import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;



import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GalleryScrollView extends FragmentActivity implements OnClickListener {

	static TextView  mDotstext[];
	private int mDotsCount;
	private LinearLayout mDotsLayout;
	private ArrayList<String> img_Array=new ArrayList<String>();
	InitImages images;
	String image_name;
	int count;
	ImageView more;
	MediaPlayer media;
	Gallery gallery=null;

	ImageView homeImgButton;
	ImageView sevenOneImageButton;
	ImageView porjectImageButton;
	ImageView contactImageButton;

	ImageView projectImageView;
	/*
	 * Pager For Gallery
	 */
	ViewPager mPager;
	Context context;

	Bitmap bm;
	LinearLayout				layout;
	TextView[]      			text;
	Typeface 					tf;

	ArrayList<Project> projectsList = null;
	ArrayList<Project> 	projectsListData = new ArrayList<Project>();
	ArrayList<Project> 	ListData = new ArrayList<Project>();

	Intent web=new Intent(Intent.ACTION_VIEW);

	private  InputStream is;
	private  String file_url = "http://dl.dropbox.com/u/102702781/ariisto_projects.xml";

	AriistoLinksData ariistoLink;
	Animation anim;

	File externalFile;

	private ProgressDialog pDialog;
	public static final int progress_bar_type = 0; 

	BroadcastReceiver mIntentRecevier;
	public static boolean brodCastFlag=false;
	DownloadReceiver down=new DownloadReceiver();
	IntentFilter intentFilter;
	ProjectPager projectAdapter;

	ArrayList<String>android_links;
	ArrayList<String>android_links_names;
	
	String imgeName,url;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gallery_scroll_view);


		context=this;
		layout 		= (LinearLayout) findViewById(R.id.linear_text_projects);
		tf 			= Typeface.createFromAsset(getResources().getAssets(),"fonts/bookman.ttf");


		externalFile = new File(Environment.getExternalStorageDirectory(),"CloudProjects.xml");

		ariistoLink=new AriistoLinksData(getApplicationContext());
		android_links=new ArrayList<String>();
		android_links_names=new ArrayList<String>();

		images=new InitImages();

		img_Array.add("sapphire");


		img_Array.add("solitaire");
		img_Array.add("ria");

		img_Array.add("centrum");

		img_Array.add("heaven");
		img_Array.add("county");

		img_Array.add("chambers");
		img_Array.add("sevenone");
		img_Array.add("glory");

		img_Array.add("cozy");
		img_Array.add("horizon");

		text = new TextView[img_Array.size()];
		/*
		 * ViewPager For Gallery
		 * 
		 */
		mPager=(ViewPager)findViewById(R.id.viewProject);


		if(externalFile.exists())
		{
			/*externalFile.delete();*/
			/*Log.i("Deleted ", "In On Create");*/
			/*ariistoLink.getProjectLinks();*/

			File externalFile = new File(Environment.getExternalStorageDirectory(),"CloudProjects.xml");
			try{


				InputStream inputStream = null;

				inputStream=new FileInputStream(externalFile);
				if(inputStream != null){
					Log.d("file ","Availble");
					projectsListData = SAXXMLParser.parse(inputStream);
					brodCastFlag=true;
					projectAdapter=null;

					for(int i=0; i<projectsListData.size();i++)
					{
						Log.d("Project IN BroadCast "+projectsListData.get(i).getName()," " +projectsListData.get(i).getAndroidLink());
						android_links.add(projectsListData.get(i).getAndroidLink());
						android_links_names.add(projectsListData.get(i).getName());
						/*Toast.makeText(getApplicationContext(), "Project "+projectsListData.get(i).getName()+" Link: \n" +projectsListData.get(i).getAndroidLink(),Toast.LENGTH_SHORT).show();*/
					}
					projectAdapter=new ProjectPager(getSupportFragmentManager(), context, img_Array,android_links,android_links_names);

					layout.removeAllViews();
					for(int i = 0; i <img_Array.size() ; i++){
						text[i] = new TextView(context);
						text[i].setTextSize(45f);
						text[i].setTextColor(Color.GRAY);
						text[i].setText(".");
						text[i].setTypeface(tf);

						layout.addView(text[i]);	
					}
					text[0].setTextColor(Color.WHITE);
					mPager.setAdapter(projectAdapter);
					ListData=projectsListData;

				}	
				inputStream.close();

			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		else
		{
			ariistoLink.getProjectLinks();
			layout.removeAllViews();
			for(int i = 0; i <img_Array.size() ; i++){
				text[i] = new TextView(context);
				text[i].setTextSize(45f);
				text[i].setTextColor(Color.GRAY);
				text[i].setText(".");
				text[i].setTypeface(tf);

				layout.addView(text[i]);	
			}
			text[0].setTextColor(Color.WHITE);
			projectAdapter=new ProjectPager(getSupportFragmentManager(), context, img_Array,android_links,android_links_names);
		}
		//NEed to be tested!

		if(brodCastFlag && projectsListData.size()!=0){
			layout.removeAllViews();
			for(int i = 0; i <img_Array.size() ; i++){
				text[i] = new TextView(context);
				text[i].setTextSize(45f);
				text[i].setTextColor(Color.GRAY);
				text[i].setText(".");
				text[i].setTypeface(tf);

				layout.addView(text[i]);	
			}
			text[0].setTextColor(Color.WHITE);
			mPager.setAdapter(projectAdapter);
		}
		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				for(int i=0;i<text.length;i++)
				{
					if(i==arg0)
					{
						text[i].setTextColor(Color.WHITE);
					}
					else
					{
						text[i].setTextColor(Color.GRAY);
					}
				}

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub


			}

			@Override
			public void onPageScrollStateChanged(int state) {
				// TODO Auto-generated method stub

				if(state==2)
				{
					media.start();
				}
				if(state==1)
				{
					ProjectPagerGallery.more.setVisibility(View.INVISIBLE);
				}
				if(state==0)
				{
					ProjectPagerGallery.more.setVisibility(View.VISIBLE);
				}
			}
		});




		homeImgButton=(ImageView)findViewById(R.id.btn1);
		sevenOneImageButton=(ImageView)findViewById(R.id.btn2);
		porjectImageButton=(ImageView)findViewById(R.id.btn3);
		contactImageButton=(ImageView)findViewById(R.id.btn4);

		porjectImageButton.setBackgroundResource(R.drawable.projects_act);


		homeImgButton.setOnClickListener(this);
		sevenOneImageButton.setOnClickListener(this);
		porjectImageButton.setOnClickListener(this);
		contactImageButton.setOnClickListener(this);

		media=MediaPlayer.create(getApplicationContext(),R.raw.photoslidesound);

		intentFilter = new IntentFilter(getPackageName()+getLocalClassName());


	}




	class DownloadReceiver extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			//Parsing when file has been successfully downloaded 
			String res =intent.getStringExtra("Downloaded");
			if(res.equalsIgnoreCase("yes"))
			{

				File externalFile = new File(Environment.getExternalStorageDirectory(),"CloudProjects.xml");
				try{

					AssetManager asset =getResources().getAssets();
					InputStream inputStream = null;
					/*inputStream = asset.open("CloudProjects.xml");*/
					inputStream=new FileInputStream(externalFile);
					if(inputStream != null){
						Log.d("file ","Availble");
						projectsListData = SAXXMLParser.parse(inputStream);
						brodCastFlag=true;
						projectAdapter=null;

						for(int i=0; i<projectsListData.size();i++)
						{
							Log.d("Project IN BroadCast "+projectsListData.get(i).getName()," " +projectsListData.get(i).getAndroidLink());
							android_links.add(projectsListData.get(i).getAndroidLink());
							android_links_names.add(projectsListData.get(i).getName());
							/*Toast.makeText(getApplicationContext(), "Project "+projectsListData.get(i).getName()+" Link: \n" +projectsListData.get(i).getAndroidLink(),Toast.LENGTH_SHORT).show();*/
						}
						projectAdapter=new ProjectPager(getSupportFragmentManager(), context, img_Array,android_links,android_links_names);
						mPager.setAdapter(projectAdapter);
						ListData=projectsListData;

					}	
					inputStream.close();

				}catch(IOException e){
					e.printStackTrace();
				}


			}
			/*if progress is aborted or file is not being downloaded from server
			 *then parsing data from local resources
			 */
			/*if(res.equalsIgnoreCase("no"))*/
			else
			{

				try{



					for(int i=0; i<img_Array.size();i++)
					{

						android_links.add("null");
						android_links_names.add("null");

						//Toast.makeText(getApplicationContext(), "Project "+projectsListData.get(i).getName()+" Link: \n" +projectsListData.get(i).getAndroidLink(),Toast.LENGTH_SHORT).show();
					}
					projectAdapter=null;
					projectAdapter=new ProjectPager(getSupportFragmentManager(), context, img_Array,android_links,android_links_names);
					mPager.setAdapter(projectAdapter);

					ListData=projectsListData;


				}catch(Exception e){
					e.printStackTrace();
				}
			}


		}

	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		registerReceiver(down, intentFilter);
		super.onResume();
	}




	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		unregisterReceiver(down);
		super.onPause();
	}


	@Override
	protected Dialog onCreateDialog(int id)
	{
		switch (id)
		{
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}

	public void recycleBitmap()
	{
		if(bm!=null)
		{
			bm.recycle();

			bm=null;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(externalFile.exists())
		{
			externalFile.delete();
		}
		this.finish();
		int pid = android.os.Process.myPid();
		android.os.Process.killProcess(pid);
	}


	public void setImages(String image_name,int count)
	{
		for(int i=0;i<count;i++)
		{

			images.setImages(image_name+(i+1));
			Log.d("Image",": "+images.getImageName(i));
		}
	}


	public  class ProjectPager extends FragmentStatePagerAdapter
	{

		Context context;
		ArrayList<String> projectName=null;
		ArrayList<String> urls=null;
		ArrayList<String> names=null;
		ArrayList<String> andlink=new ArrayList<String>();;

		ArrayList<String> urlsCopy=null;
		ArrayList<String> namesCopy=null;
		boolean flagbreak=false;
		int searchpos;
		public ProjectPager(FragmentManager fm,Context context,ArrayList<String>projectName,ArrayList<String>urls,ArrayList<String>names) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.projectName=projectName;
			this.urls=urls;
			this.names=names;
			this.urlsCopy=urls;
			this.namesCopy=names;


			if(urls.size()==0)
			{
				for(int i=0;i<img_Array.size();i++)
				{

				}
			}

			for(int i=0;i<projectName.size();i++)
			{
				andlink.add("null");
			}

			for(int i=0;i<names.size();i++)
			{
				Log.i("Name : "," "+names.get(i));
				if(flagbreak)
				{
					break;
				}
				for(int j=0;j<projectName.size();j++)
				{
					Log.i("Project Name : "," "+projectName.get(j));
					if(projectName.get(j).equalsIgnoreCase(names.get(i)))
					{
						Log.i("Android link : "," "+urls.get(i));
						/*	andlink.add(j, urls.get(i));*/
						/*	andlink[j]=urls.get(i);*/

						andlink.set(j,urls.get(i));
						urlsCopy.remove(i);
						namesCopy.remove(i);
						searchpos=j;
						flagbreak=true;
						break;

					}
					else
					{
						/*andlink.add(j, "null");*/
						/*	andlink[j]="nolink";*/

						andlink.set(j,"null");
					}
				}

			}//for loop ends here


			for(int i=0;i<namesCopy.size();i++)
			{

				for(int j=0;j<projectName.size();j++)
				{
					Log.i("Project Name in Andlink: "," "+projectName.get(j));
					if(projectName.get(j).equalsIgnoreCase(namesCopy.get(i)))
					{

						if(andlink.get(j).equalsIgnoreCase("null"))
						{
							andlink.set(j, urlsCopy.get(i));
							Log.i("AndLink in Andlink: "," "+andlink.get(j));
						}


					}

				}

			}//for loop ends here



		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stubs

			/*image_name=projectName.get(position);
			url=andlink.get(position);*/

			/*return new ProjectPagerGallery(context, projectName.get(position),urls.get(position),names.get(position));*/
			return new ProjectPagerGallery(context, projectName.get(position),andlink.get(position));
			/*return new ProjectPagerGallery(context, image_name,url);*/

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return projectName.size();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub

			/*	FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();
			 */
			super.destroyItem(container, position, object);
		}



	}
	
	
	


	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		if (outState.isEmpty()) {

			outState.putString("ImageName", imgeName);
			outState.putString("ImageUrl", url);
		}


	}
	
	




	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onRestoreInstanceState(savedInstanceState);
		if(savedInstanceState != null){
			imgeName = savedInstanceState.getString("ImageName");
			url = savedInstanceState.getString("ImageUrl");
		}
	}




	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_gallery_scroll_view, menu);
		return false;
	}



	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub


		mDotsLayout=null;
		img_Array=null;
		images=null;
		gallery=null;
		more=null;
		media=null;
		super.onDestroy();
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if(homeImgButton.getId()==v.getId())
		{

			Intent intent=new Intent(getApplicationContext(),Home.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();

		}
		if(sevenOneImageButton.getId()==v.getId())
		{

			Intent intent=new Intent(getApplicationContext(),Projects.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();

		}
		if(contactImageButton.getId()==v.getId())
		{

			Intent intent=new Intent(getApplicationContext(),ContactUs.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();

		}

	}







	//Parsing..


	public class AriistoLinksData {
		Context context;

		ArrayAdapter<String> adapter ;
		int id;
		NetworkCheck network;



		public AriistoLinksData(Context context)
		{
			this.context=context;
			network= new NetworkCheck(context);
			projectsList = new ArrayList<Project>();

		}//end of CTOR

		/*	public ArrayList<Project> getProjectLinks()*/
		public void getProjectLinks()
		{

			File externalFile = new File(Environment.getExternalStorageDirectory(),"CloudProjects.xml");
			if(network.isNetworkAvailable())
			{
				Log.d("INTERNET ","//************* " + "Internet Availble" + " ***********/*/");

				// call asynch task
				try
				{
					new Parsing().execute(file_url);

				}
				catch(Exception ex)
				{

					ex.printStackTrace();
				}



			}
			else
			{
				try{



					for(int i=0; i<img_Array.size();i++)
					{

						android_links.add("null");
						android_links_names.add("null");

					}
					projectAdapter=null;
					projectAdapter=new ProjectPager(getSupportFragmentManager(), context, img_Array,android_links,android_links_names);
					mPager.setAdapter(projectAdapter);

				}catch(Exception e){
					e.printStackTrace();
				}
			}



			for(int i=0; i<android_links.size();i++)
			{
				Log.d("Project ArristoFun "+android_links.get(i)," ArristoFun Links " +android_links.get(i));
			}
			Log.d("Callback ","Came Here"+projectsList.size());



		}

		public ArrayList<Project> getProjectList()
		{
			return projectsList;
		}

		class Parsing extends AsyncTask<String, String, ArrayList<Project>> {
			/*ArrayList<Project> projectsList = new ArrayList<Project>();*/
			URL url;

			@Override
			protected ArrayList<Project> doInBackground(String...file_url ) {
				// TODO Auto-generated method stub
				int count;
				ArrayList<Project> project=new ArrayList<Project>();
				/*			ArrayList<Project> projectsList = new ArrayList<Project>();*/
				/*	Log.d("", "DO IN BACKGROUND");*/
				try{
					URL url = new URL(file_url[0]);
					URLConnection connection = url.openConnection();
					connection.connect();
					int lenghtOfFile = connection.getContentLength();
					// input stream to read file - with 8k buffer
					InputStream input = new BufferedInputStream(url.openStream(), 8192);
					// Output stream to write file
					FileOutputStream output = new FileOutputStream("/sdcard/CloudProjects.xml");
					byte data[] = new byte[1024];
					while ((count = input.read(data)) != -1) 
					{
						/*	Log.d("External File", "DELETED");*/
						// publishing the progress....
						// After this onProgressUpdate will be called
						// writing data to file
						output.write(data, 0, count);
						Log.d("Writting has been finished "," ");
					}




					output.flush();
					// closing streams
					output.close();
					input.close();
				}
				catch (Exception e)
				{
					Log.e("Error: ", e.getMessage());

					Log.i("File not found in Async Task"," File not found here");
					Intent intent=new Intent();
					intent.setAction(getPackageName()+getLocalClassName());
					intent.putExtra("Downloaded","no");
					sendBroadcast(intent);

					try{


						for(int i=0; i<img_Array.size();i++)
						{

							android_links.add("null");
							android_links_names.add("null");

							//Toast.makeText(getApplicationContext(), "Project "+projectsListData.get(i).getName()+" Link: \n" +projectsListData.get(i).getAndroidLink(),Toast.LENGTH_SHORT).show();
						}
						projectAdapter=null;
						projectAdapter=new ProjectPager(getSupportFragmentManager(), context, img_Array,android_links,android_links_names);
						mPager.setAdapter(projectAdapter);



					}catch(Exception ex){
						ex.printStackTrace();
					}
				}

				return null;
			}

			@Override
			protected void onCancelled() {
				// TODO Auto-generated method stub
				super.onCancelled();
				Log.d("isCancled","Called");
				File externalFile = new File(Environment.getExternalStorageDirectory(),"CloudProjects.xml");
				if(externalFile.exists()){
					externalFile.delete();

				}
				Intent intent=new Intent();
				intent.setAction(getPackageName()+getLocalClassName());
				intent.putExtra("Downloaded","no");
				sendBroadcast(intent);


			}

			@Override
			protected void onPostExecute(ArrayList<Project> result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);

				removeDialog(progress_bar_type);

				Intent intent=new Intent();
				intent.setAction(getPackageName()+getLocalClassName());
				intent.putExtra("Downloaded","yes");
				sendBroadcast(intent);

			}
			protected void onProgressUpdate(String... progress) 
			{
				// setting progress percentage
				pDialog.setProgress(Integer.parseInt(progress[0]));


			}
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				showDialog(progress_bar_type);

			}

		}

		public void setData(ArrayList<Project> res)
		{
			projectsListData.clear();
			projectsListData.addAll(res);
		}

	}//end of AriistoLinksData











}
