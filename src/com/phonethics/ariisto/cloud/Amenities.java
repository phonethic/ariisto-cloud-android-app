package com.phonethics.ariisto.cloud;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

public class Amenities extends Activity implements OnClickListener, OnTouchListener {
	/*
	private ImageView 		info_icon;
	private boolean 		flag;
	InitImages 				images;
	String 					image_name;
	int 					count;
	Gallery 				gallery;

	ImageView 				projectImageView;

	Bitmap 					bm;

	private ToggleButton 	tog_button;

	ArrayList<String> info_text			=	new ArrayList<String>();
	private ArrayList<String> img_Array	=	new ArrayList<String>();

	MediaPlayer 			media;


	 */

	private ToggleButton 	tog_button;
	ImageView 				imgDayNight;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_amenities);

		/*media		=	MediaPlayer.create(getApplicationContext(), R.raw.photoslidesound);*/
		/*gallery		=	(Gallery)findViewById(R.id.gallery);

		tog_button 	= (ToggleButton) findViewById(R.id.toggleAmenities);
		tog_button.setOnClickListener(this);
		tog_button.setOnTouchListener(this);

		images		=	new InitImages();
		info_icon	=	(ImageView)findViewById(R.id.info_icon);

		flag		=	false;

		setImages("internal",3);
		img_Array.add("internal_amenities");
		gallery.setAdapter(new GalleryAminitesImageAdapter(getApplicationContext(),img_Array));
		recycleBitmap();

		gallery.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				return false;
			}
		});*/

		tog_button 		= (ToggleButton) findViewById(R.id.toggleAmenities);
		imgDayNight 	= (ImageView)	findViewById(R.id.imageDayNight);
		tog_button.setOnClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_amenities, menu);
		return false;
	}

	@Override
	public void onClick(View v) {
		if(tog_button.isChecked() ){
			imgDayNight.setBackgroundResource(R.drawable.external_amenities);			
		}else{
			imgDayNight.setBackgroundResource(R.drawable.internal_amenities);
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}
/*
	public void recycleBitmap(){
		if(bm!=null){
			bm.recycle();
			bm=null;
		}
	}
/*
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(tog_button.isChecked()){
			flag=false;
			img_Array.clear();
			img_Array.add("external_amenities");
			gallery.setAdapter(new GalleryAminitesImageAdapter(getApplicationContext(),img_Array));

		}else{
			flag=true;
			img_Array.clear();
			img_Array.add("internal_amenities");
			setInfoText(info_text,info_text.size());
			gallery.setAdapter(new GalleryAminitesImageAdapter(getApplicationContext(),img_Array));
		}
	}

	//setting images for Gallery
	public void setImages(String image_name,int count){
		images.clearImages();
		for(int i=0;i<count;i++){
			images.setImages(image_name+(i+1));
			Log.d("Image",": "+images.getImageName(i));
		}
	}

	//setting text for external amenities

	public void setInfoText(ArrayList<String> info_text2,int count){
		images.clearInfoText();
		for(int i=0;i<count;i++){
			Log.d("info Text: "+ images.getImageName(i)," "+info_text2.get(i) );
			images.setInfoText(images.getImageName(i),info_text.get(i));

		}
	}
	 	//Adding Image Adapter for Amenities

	public class GalleryAminitesImageAdapter extends BaseAdapter {
		private Context 							aContext;
		private ArrayList<String> mImageNames	=	new ArrayList<String>();
		int count								=	0;

		public GalleryAminitesImageAdapter(Context c,ArrayList<String> arr){
			mImageNames.addAll(arr);
			aContext=c;
		}



		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mImageNames.size();
		}

		@Override
		public Object getItem(int i) {
			// TODO Auto-generated method stub
			return i;
		}

		@Override
		public long getItemId(int i) {
			// TODO Auto-generated method stub
			return i;
		}


		final RelativeLayout 	relative		=	(RelativeLayout)findViewById(R.id.details_layout);
		final Gallery 			gallery			=	(Gallery)findViewById(R.id.gallery);

		@SuppressWarnings("deprecation")
		@Override
		public View getView( int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			if(convertView==null){
				projectImageView=new ImageView(aContext);	

			}else{
				projectImageView=(ImageView)convertView;
			}
			projectImageView.setBackgroundColor(Color.BLACK);
			projectImageView.setScaleType(ScaleType.FIT_XY);

			bm	=	bitmap(mImageNames.get(position));
			projectImageView.setImageBitmap(bm);
			projectImageView.setLayoutParams(new Gallery.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
			projectImageView.setLayoutParams(new Gallery.LayoutParams(320,380));
			final int infoposition=position ;
			info_icon.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(flag==true){
						count++;
						int pos=0;
						relative.setVisibility(ViewGroup.VISIBLE);
						relative.getBackground().setAlpha(180);
						final TextView txt	=	(TextView)findViewById(R.id.textView1);
						Log.d("Current Position",":"+pos);
						Log.d("Info Position:"+images.getInfoText(images.getImageName(infoposition))," "+infoposition);

						Log.d("Count:",":"+count);
						if(count%2!=0){
							Animation animate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.textinfo_slide_up);

							relative.startAnimation(animate);
							relative.setAnimation(animate);
							animate.setFillAfter(true);
						}
						if(count%2==0)
						{
							Animation animate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.textinfo_slide_out);
							relative.startAnimation(animate);
							animate.setFillAfter(true);
							relative.setAnimation(animate);
						}
					}
					else
					{
						Intent intent=new Intent(getApplicationContext(),ShowInfoImage.class);
						intent.putExtra("img_name",images.getInfoImageName(infoposition));
						startActivity(intent);
					}

				}
			});

			return projectImageView;
		}

		private Bitmap bitmap(String name)
		{
			final Bitmap bitmap=BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(name, "drawable", getPackageName()));
			return bitmap;
		}
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		info_icon=null;
		images=null;
		System.gc();
		super.onDestroy();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		if(tog_button.isChecked())
		{

			flag=false;
			img_Array.clear();
			img_Array.add("externalamenities_back");


			gallery.setAdapter(new GalleryAminitesImageAdapter(getApplicationContext(),img_Array));

		}
		else
		{
			flag=true;
			img_Array.clear();
			img_Array.add("internalamenities_back");
			setInfoText(info_text,info_text.size());
			gallery.setAdapter(new GalleryAminitesImageAdapter(getApplicationContext(),img_Array));
		}
		return false;
	}
	 */
}
