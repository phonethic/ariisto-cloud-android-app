package com.phonethics.ariisto.cloud;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView.ScaleType;

public class GallerScrollView extends Activity implements OnClickListener, OnItemSelectedListener {


	static TextView  mDotstext[];
	private int mDotsCount;
	private LinearLayout mDotsLayout;
	private ArrayList<String> img_Array=new ArrayList<String>();
	InitImages images;
	String image_name;
	int count;
	ImageView more;
	MediaPlayer media;
	Gallery gallery=null;

	ImageView homeImgButton;
	ImageView sevenOneImageButton;
	ImageView porjectImageButton;
	ImageView contactImageButton;

	ImageView projectImageView;
	Bitmap bm;
	
	LinearLayout				layout;
	TextView[]      			text;
	Typeface 					tf;

	ArrayList<Project> projectsList = null;
	ArrayList<Project> 	projectsListData = new ArrayList<Project>();
	ArrayList<Project> 	ListData = new ArrayList<Project>();

	Intent web=new Intent(Intent.ACTION_VIEW);

	private  InputStream is;
	/*private  String file_url = "http://192.168.254.13/ariisto_projects.xml";*/
	private  String file_url = "http://dl.dropbox.com/u/102702781/ariisto_projects.xml";

	AriistoLinksData ariistoLink;
	Animation anim;

	File externalFile;

	private ProgressDialog pDialog;
	public static final int progress_bar_type = 0; 

	BroadcastReceiver mIntentRecevier;
	public static boolean brodCastFlag=false;
	DownloadReceiver down=new DownloadReceiver();
	IntentFilter intentFilter;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_galler_scroll_view);

		
		layout 		= (LinearLayout) findViewById(R.id.linear_text_projects);
		tf 			= Typeface.createFromAsset(getResources().getAssets(),"fonts/bookman.ttf");
		
		gallery=(Gallery)findViewById(R.id.gallery);

		anim=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
		/*final Animation anim=new AnimationUtils().loadAnimation(getApplicationContext(), R.anim.fade_in);*/
		externalFile = new File(Environment.getExternalStorageDirectory(),"projects.xml");

		ariistoLink=new AriistoLinksData(getApplicationContext());

		/*
		 * Commented temporarily
		 * 
		 */
		try
		{
			if(externalFile.exists())
			{
				externalFile.delete();
				ariistoLink.getProjectLinks();
			}
			else
			{
				ariistoLink.getProjectLinks();
			}
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}

		images=new InitImages();
		more=(ImageView)findViewById(R.id.imgClick);

		img_Array.add("sapphire");
		/*img_Array.add("cloud");*/


		img_Array.add("solitaire");
		img_Array.add("ria");

		img_Array.add("centrum");
		img_Array.add("heaven");

		img_Array.add("county");
		img_Array.add("chambers");

		img_Array.add("sevenone");
		img_Array.add("glory");

		img_Array.add("cozy");
		img_Array.add("horizon");


		homeImgButton=(ImageView)findViewById(R.id.btn1);
		sevenOneImageButton=(ImageView)findViewById(R.id.btn2);
		porjectImageButton=(ImageView)findViewById(R.id.btn3);
		contactImageButton=(ImageView)findViewById(R.id.btn4);

		porjectImageButton.setImageResource(R.drawable.projects_act);

		gallery.setAdapter(new GalleryImageAdapter(getApplicationContext(),img_Array));

		recycleBitmap();

		homeImgButton.setOnClickListener(this);
		sevenOneImageButton.setOnClickListener(this);
		porjectImageButton.setOnClickListener(this);
		contactImageButton.setOnClickListener(this);

		media=MediaPlayer.create(getApplicationContext(),R.raw.photoslidesound);

		intentFilter = new IntentFilter(getPackageName()+getLocalClassName());
		gallery.setOnItemSelectedListener(this);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();

	}
	class DownloadReceiver extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			//Parsing when file has been successfully downloaded 
			String res =intent.getStringExtra("Downloaded");
			if(res.equalsIgnoreCase("yes"))
			{

				File externalFile = new File(Environment.getExternalStorageDirectory(),"projects.xml");
				try{

					AssetManager asset =getResources().getAssets();
					InputStream inputStream = null;
					/*inputStream = asset.open("projects.xml");*/
					inputStream=new FileInputStream(externalFile);
					if(inputStream != null){
						Log.d("file ","Availble");
						projectsListData = SAXXMLParser.parse(inputStream);
						brodCastFlag=true;
						for(int i=0; i<projectsListData.size();i++)
						{
							Log.d("Project "+projectsListData.get(i).getName()," " +projectsListData.get(i).getAndroidLink());
							/*Toast.makeText(getApplicationContext(), "Project "+projectsListData.get(i).getName()+" Link: \n" +projectsListData.get(i).getAndroidLink(),Toast.LENGTH_SHORT).show();*/
						}
						ListData=projectsListData;

					}	
					inputStream.close();

				}catch(IOException e){
					e.printStackTrace();
				}


			}
			/*if progress is aborted or file is not being downloaded from server
			 *then parsing data from local resources
			 */
			/*if(res.equalsIgnoreCase("no"))*/
			else
			{

				try{

					AssetManager asset =getResources().getAssets();
					InputStream inputStream = null;
					inputStream = asset.open("projects.xml");
					if(inputStream != null){
						Log.d("file ","Availble");
						projectsListData = SAXXMLParser.parse(getAssets().open("projects.xml"));
						brodCastFlag=true;
						for(int i=0; i<projectsListData.size();i++)
						{
							Log.d("Project "+projectsListData.get(i).getName()," " +projectsListData.get(i).getAndroidLink());
							//Toast.makeText(getApplicationContext(), "Project "+projectsListData.get(i).getName()+" Link: \n" +projectsListData.get(i).getAndroidLink(),Toast.LENGTH_SHORT).show();
						}
						ListData=projectsListData;

					}	

				}catch(IOException e){
					e.printStackTrace();
				}
			}


		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		registerReceiver(down, intentFilter);
		super.onResume();
	}




	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		unregisterReceiver(down);
		super.onPause();
	}


	@Override
	protected Dialog onCreateDialog(int id)
	{
		switch (id)
		{
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			pDialog.setCancelable(true);
			pDialog.show();
			return pDialog;
		default:
			return null;
		}
	}

	public void recycleBitmap()
	{
		if(bm!=null)
		{
			bm.recycle();

			bm=null;
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		this.finish();
		int pid = android.os.Process.myPid();
		android.os.Process.killProcess(pid);
	}


	public void setImages(String image_name,int count)
	{
		for(int i=0;i<count;i++)
		{

			images.setImages(image_name+(i+1));
			Log.d("Image",": "+images.getImageName(i));
		}
	}

	public class GalleryImageAdapter extends BaseAdapter {

		private Context aContext;
		private ArrayList<String> mImageNames=new ArrayList<String>();
		/*		MediaPlayer media;*/
		ImageView more;

		Handler mHandler=null;
		public GalleryImageAdapter(Context c,ArrayList<String> arr)
		{
			mImageNames.addAll(arr);
			aContext=c;
			/*		media=MediaPlayer.create(aContext, R.raw.photoslidesound);
			mHandler=new Handler();*/


		}



		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mImageNames.size();
		}

		@Override
		public Object getItem(int i) {
			// TODO Auto-generated method stub
			return i;
		}

		@Override
		public long getItemId(int i) {
			// TODO Auto-generated method stub
			return i;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null)
			{
				projectImageView=new ImageView(aContext);	

			}
			else
			{
				projectImageView=(ImageView)convertView;

			}
			projectImageView.setBackgroundColor(Color.BLACK);
			projectImageView.setScaleType(ScaleType.FIT_XY);
			bm=bitmap(mImageNames.get(position));
			projectImageView.setImageBitmap(bm);
			projectImageView.setLayoutParams(new Gallery.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

			return projectImageView;


		}

		private Bitmap bitmap(String name)
		{
			final Bitmap bitmap=BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(name, "drawable", getPackageName()));
			return bitmap;
		}


	}//end of inner class


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_galler_scroll_view, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub


		mDotsLayout=null;
		img_Array=null;
		images=null;
		gallery=null;
		more=null;
		media=null;
		super.onDestroy();
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if(homeImgButton.getId()==v.getId())
		{

			Intent intent=new Intent(getApplicationContext(),Home.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();

		}
		if(sevenOneImageButton.getId()==v.getId())
		{

			Intent intent=new Intent(getApplicationContext(),Projects.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();

		}
		if(contactImageButton.getId()==v.getId())
		{

			Intent intent=new Intent(getApplicationContext(),ContactUs.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();

		}

	}


	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1,final int position,
			long arg3) {
		// TODO Auto-generated method stub
		/*		arg1.startAnimation(anim);*/
		/*	more.setVisibility(View.GONE);*/

		more.setVisibility(View.VISIBLE);

		if((position==0))
		{

			more.setVisibility(View.VISIBLE);
			more.setImageResource(R.drawable.comingsoon);
			more.setScaleType(ScaleType.FIT_XY);
		}
		else if(position == 1)
		{
			more.setVisibility(View.GONE);
			
		}else if(position == 2){
			more.setVisibility(View.VISIBLE);
			more.setImageResource(R.drawable.download);
			more.setScaleType(ScaleType.FIT_XY);
		}else{
			more.setVisibility(View.VISIBLE);
			more.setImageResource(R.drawable.comingsoon);
			more.setScaleType(ScaleType.FIT_XY);
		}
		
		if(brodCastFlag){
			try{
			if((ListData.get(position).getName().equalsIgnoreCase("cloud")) && (img_Array.get(position).equalsIgnoreCase("cloud")) ){
				more.setVisibility(View.GONE);
			}else{
				if((ListData.get(position).getName()).equalsIgnoreCase(img_Array.get(position)) && !(ListData.get(position).getAndroidLink().equalsIgnoreCase("null"))){
					more.setVisibility(View.VISIBLE);
					more.setImageResource(R.drawable.download);
					more.setScaleType(ScaleType.FIT_XY);
				}else{
					more.setVisibility(View.VISIBLE);
					more.setImageResource(R.drawable.comingsoon);
					more.setScaleType(ScaleType.FIT_XY);
				}
			}
			}catch(IndexOutOfBoundsException ex){
				
			}
		}



		more.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(brodCastFlag)
				{
					/*if((ListData.get(position).getName().equalsIgnoreCase("cloud")) && (img_Array.get(position).equalsIgnoreCase("cloud")) ){
					more.setVisibility(View.GONE);
					}else{
						if((ListData.get(position).getName()).equalsIgnoreCase(img_Array.get(position)) && !(ListData.get(position).getAndroidLink().equalsIgnoreCase("null"))){
							more.setVisibility(View.VISIBLE);
							more.setImageResource(R.drawable.download);
							more.setScaleType(ScaleType.FIT_XY);
						}else{
							more.setVisibility(View.VISIBLE);
							more.setImageResource(R.drawable.comingsoon);
							more.setScaleType(ScaleType.FIT_XY);
						}
					}*/

					if((ListData.get(position).getName()).equalsIgnoreCase(img_Array.get(position)) && !(ListData.get(position).getAndroidLink().equalsIgnoreCase("null")))
					{
						web.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.imdb.mobile"));
						startActivity(web);

						Log.d("WEB LINK", ListData.get(position).getAndroidLink());
					}

				}
			}
		});
		if(position>0)
		{
			media.start();
		}

	}


	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}



	//Parsing..


	public class AriistoLinksData {
		Context context;

		ArrayAdapter<String> adapter ;
		int id;
		NetworkCheck network;



		public AriistoLinksData(Context context)
		{
			this.context=context;
			network= new NetworkCheck(context);
			projectsList = new ArrayList<Project>();

		}//end of CTOR

		/*	public ArrayList<Project> getProjectLinks()*/
		public void getProjectLinks()
		{

			File externalFile = new File(Environment.getExternalStorageDirectory(),"projects.xml");
			if(network.isNetworkAvailable())
			{
				Log.d("INTERNET ","//************* " + "Internet Availble" + " ***********/*/");

				// call asynch task
				try
				{
					new Parsing().execute(file_url);

				}
				catch(Exception ex)
				{

					ex.printStackTrace();
				}


				String path = Environment.getExternalStorageDirectory() + "/projects.xml";
				try {
					is = new FileInputStream((Environment.getExternalStorageDirectory()).toString()+"/projects.xml");
					File file = new File(path);
					if(file.exists()){
						Log.d("FILE Present", "FILE PRESENT :" + file);
					}

				} catch (FileNotFoundException e) {

					e.printStackTrace();
				}
			}
			else
			{
				try{

					AssetManager asset =getResources().getAssets();
					InputStream inputStream = null;
					inputStream = asset.open("projects.xml");
					if(inputStream != null){
						Log.d("file ","Availble");
						projectsListData = SAXXMLParser.parse(getAssets().open("projects.xml"));
						brodCastFlag=true;
						for(int i=0; i<projectsListData.size();i++)
						{
							Log.d("Project "+projectsListData.get(i).getName()," " +projectsListData.get(i).getAndroidLink());
							/*Toast.makeText(getApplicationContext(), "Project "+projectsListData.get(i).getName()+" Link: \n" +projectsListData.get(i).getAndroidLink(),Toast.LENGTH_SHORT).show();*/
						}
						ListData=projectsListData;

					}	

				}catch(IOException e){
					e.printStackTrace();
				}
			}



			for(int i=0; i<projectsList.size();i++)
			{
				Log.d("Project ArristoFun "+projectsList.get(i).getName()," ArristoFun Links " +projectsList.get(i).getAndroidLink());
			}
			Log.d("Callback ","Came Here"+projectsList.size());

			/*		return projectsList;*/

		}

		public ArrayList<Project> getProjectList()
		{
			return projectsList;
		}

		class Parsing extends AsyncTask<String, String, ArrayList<Project>> {
			/*ArrayList<Project> projectsList = new ArrayList<Project>();*/
			URL url;

			@Override
			protected ArrayList<Project> doInBackground(String...file_url ) {
				// TODO Auto-generated method stub
				int count;
				ArrayList<Project> project=new ArrayList<Project>();
				/*			ArrayList<Project> projectsList = new ArrayList<Project>();*/
				/*	Log.d("", "DO IN BACKGROUND");*/
				try{
					URL url = new URL(file_url[0]);
					URLConnection connection = url.openConnection();
					connection.connect();
					int lenghtOfFile = connection.getContentLength();
					// input stream to read file - with 8k buffer
					InputStream input = new BufferedInputStream(url.openStream(), 8192);
					// Output stream to write file
					FileOutputStream output = new FileOutputStream("/sdcard/projects.xml");
					byte data[] = new byte[1024];
					while ((count = input.read(data)) != -1) 
					{
						/*	Log.d("External File", "DELETED");*/
						// publishing the progress....
						// After this onProgressUpdate will be called
						// writing data to file
						output.write(data, 0, count);
						Log.d("Writting has been finished "," ");
					}




					output.flush();
					// closing streams
					output.close();
					input.close();
				}
				catch (Exception e)
				{
					Log.e("Error: ", e.getMessage());

					Log.i("File not found in Async Task"," File not found here");
					Intent intent=new Intent();
					intent.setAction(getPackageName()+getLocalClassName());
					intent.putExtra("Downloaded","no");
					sendBroadcast(intent);
				}

				return null;
			}

			@Override
			protected void onCancelled() {
				// TODO Auto-generated method stub
				super.onCancelled();
				Log.d("isCancled","Called");
				File externalFile = new File(Environment.getExternalStorageDirectory(),"projects.xml");
				if(externalFile.exists()){
					externalFile.delete();

				}
				Intent intent=new Intent();
				intent.setAction(getPackageName()+getLocalClassName());
				intent.putExtra("Downloaded","no");
				sendBroadcast(intent);


			}

			@Override
			protected void onPostExecute(ArrayList<Project> result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);

				removeDialog(progress_bar_type);

				Intent intent=new Intent();
				intent.setAction(getPackageName()+getLocalClassName());
				intent.putExtra("Downloaded","yes");
				sendBroadcast(intent);

			}
			protected void onProgressUpdate(String... progress) 
			{
				// setting progress percentage
				pDialog.setProgress(Integer.parseInt(progress[0]));


			}
			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();
				showDialog(progress_bar_type);

			}

		}

		public void setData(ArrayList<Project> res)
		{
			projectsListData.clear();
			projectsListData.addAll(res);
		}

	}//end of AriistoLinksData

}
