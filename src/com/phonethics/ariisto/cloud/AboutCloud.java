package com.phonethics.ariisto.cloud;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class AboutCloud extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_cloud);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_about_cloud, menu);
        return false;
    }
   
    @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
    	super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}
}
