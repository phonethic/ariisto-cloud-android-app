package com.phonethics.ariisto.cloud;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ProjectPagerGallery extends Fragment {

	Context context;
	ArrayList<String>projectImage;
	String imgeName;
	public static ImageView more;
	String url="";
	Intent web;
	boolean log=true;
	/*String names;*/
	public ProjectPagerGallery()
	{

	}

	public ProjectPagerGallery(Context context,String imgeName,String url)
	{
		this.context=context;
		this.imgeName=imgeName;
		web=new Intent(Intent.ACTION_VIEW);
		this.url=url;
		/*this.names=names;*/
	}





	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if(savedInstanceState != null){
			imgeName = savedInstanceState.getString("ImageName");
			url = savedInstanceState.getString("ImageUrl");
			context=getActivity().getApplicationContext();

		}
		View view=inflater.inflate(R.layout.projectimages, container,false);
		ImageView imgView=(ImageView)view.findViewById(R.id.projectImage);


		more=(ImageView)view.findViewById(R.id.imgClick);
		if(log)
		{
			Log.i("Got in Pager URL", " : "+url);
		}

		if(url.equalsIgnoreCase("null"))
		{

			more.setImageResource(R.drawable.comingsoon);

		}

		more.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!url.equalsIgnoreCase("null") && url!=null)
				{

					web.setData(Uri.parse(url));
					startActivity(web);

				}
			}
		});
		try
		{
			imgView.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+imgeName,"drawable",context.getPackageName())));
		}
		catch(Exception ex)
		{

			ex.printStackTrace();
			
		}
		return view;
	}



	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		if(savedInstanceState != null){
			imgeName = savedInstanceState.getString("ImageName");
			url = savedInstanceState.getString("ImageUrl");
			context=getActivity().getApplicationContext();
		}
	}





	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);

		outState.putString("ImageName", imgeName);
		outState.putString("ImageUrl", url);

		context=getActivity().getApplicationContext();


	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.i("Reusme", "Called");
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		Log.i("Start", "Called");
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		Log.i("Stop", "Called");
	}






}
