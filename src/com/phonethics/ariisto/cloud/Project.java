package com.phonethics.ariisto.cloud;

import java.io.Serializable;
import java.util.ArrayList;

import android.util.Log;

public class Project implements Serializable {
	
	private String iphone_link =null;
	private String ipad_link= null;
	private String android_link="";
	private String result=null;
	private String name;
	private ArrayList<String> url = new ArrayList<String>();
	private String[] urls = new String[11];

	
	public void setName(String receivedName){
		/*Log.d("Received Project Name",receivedName);*/
		this.name = receivedName;
		Log.d("Project Name",this.name);
	}
	public String getName(){
		return name;
	}

	public String getAndroidLink(){
		Log.d("passing_adnroidLink", android_link);
		return android_link;
		/*Log.d("passing_adnroidLink", urls[i]);
		
		return urls[i];*/
	}
	public void setAndroidLink(String received_android_link)
	{
		
		Log.d("Received android_link",received_android_link);
		if(received_android_link.equals(null)){
			this.android_link = "null";
			Log.d("android_link when null ",this.android_link);
			
		}else{
			this.android_link = received_android_link;
			url.add(android_link);
			Log.d("android_link",this.android_link);
		}
		/*this.android_link = received_android_link;
		this.url.add(android_link);
		Log.d("android_link",this.android_link);*/
	}
	
	public ArrayList<String> links()
	{
		return url;
	}
	
/*	public String toString(){
		String result = "Project name : " + name + "\n" + "Android Link" + android_link;
		Log.d("final Result",result);
		return result;
		Log.d("Returned Android Link",android_link);
		return android_link;
	}*/

}

