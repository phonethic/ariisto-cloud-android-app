package com.phonethics.ariisto.cloud;



import java.io.File;
import java.util.ArrayList;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView.ScaleType;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class Projects extends Activity implements OnClickListener {
	
	InitImages 					images;
	ImageView 					more;
	static MediaPlayer 			media;
	MediaPlayer 				buttonSound;
	static Handler 				mHandler	=	null;
	Gallery 					gallery;

	ImageView 					projectImageView,homeImgButton,sevenOneImageButton,porjectImageButton,contactImageButton;
	
	Bitmap						 bm;
	File externalFile;

	private ArrayList<String> img_Array	=	new ArrayList<String>();
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projects);
        
        gallery				=	(Gallery)findViewById(R.id.projectGallery);
		images				=	new InitImages();

		homeImgButton		=	(ImageView)findViewById(R.id.btn1);
		sevenOneImageButton	=	(ImageView)findViewById(R.id.btn2);
		porjectImageButton	=	(ImageView)findViewById(R.id.btn3);
		contactImageButton	=	(ImageView)findViewById(R.id.btn4);

		sevenOneImageButton.setBackgroundResource(R.drawable.cloud_act);

		externalFile = new File(Environment.getExternalStorageDirectory(),"CloudProjects.xml");

		//adding resources
		img_Array.add("cloud");
		
		gallery.setAdapter(new GalleryProjectImageAdapter(getApplicationContext(),img_Array));

		recycleBitmap();
		homeImgButton.setOnClickListener(this);
		sevenOneImageButton.setOnClickListener(this);
		porjectImageButton.setOnClickListener(this);
		contactImageButton.setOnClickListener(this);
		
		media=MediaPlayer.create(getApplicationContext(), R.raw.drop_down_menu_sound);
		buttonSound=MediaPlayer.create(getApplicationContext(), R.raw.i_button_sound);
		more=(ImageView)findViewById(R.id.imgClick);

		
			mHandler=new Handler();

		
		gallery.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(getApplicationContext(),MoreDetails.class);
				buttonSound.start();
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				finish();

			}
		});
		
		gallery.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				/*if(position==0)
				{*/
				more.setImageResource(R.drawable.more);
				more.setVisibility(View.VISIBLE);
				more.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent=new Intent(getApplicationContext(),MoreDetails.class);
						buttonSound.start();
						startActivity(intent);
						overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
						finish();

					}
				});

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

			
    }

   
    
    public void recycleBitmap()
	{
		if(bm!=null)
		{
			bm.recycle();

			bm=null;
		}
	}
    
    @Override
	protected void onPause() {
		// TODO Auto-generated method stub
		Log.d("ProjectActivity","Pause");

		super.onPause();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		Log.d("ProjectActivity","Stop");

		super.onStop();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_projects, menu);
		return false;
	}

	public void setImages(String image_name,int count)
	{
		images.clearImages();
		for(int i=0;i<count;i++)
		{

			images.setImages(image_name+(i+1));
			Log.d("Image",": "+images.getImageName(i));
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(externalFile.exists())
		{
			externalFile.delete();
		}

		this.finish();
		int pid = android.os.Process.myPid();
		android.os.Process.killProcess(pid);
	}
	
	public class GalleryProjectImageAdapter extends BaseAdapter {
		
		private Context aContext;
		private ArrayList<String> mImageNames=new ArrayList<String>();

		public GalleryProjectImageAdapter(Context c,ArrayList<String> arr)
		{
			mImageNames.addAll(arr);
			aContext=c;

		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mImageNames.size();
		}

		@Override
		public Object getItem(int i) {
			// TODO Auto-generated method stub
			return i;
		}

		@Override
		public long getItemId(int i) {
			// TODO Auto-generated method stub
			return i;
		}
		
		@SuppressWarnings("deprecation")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub


			Log.d("ProjectActivity", ":"+position);
			/*			if(position>=0)
			{

				mHandler.post(new MyRunnable());
			}*/






			if(convertView==null)
			{
				projectImageView=new ImageView(aContext);	
			}
			else
			{

				projectImageView=(ImageView)convertView;
			}



			projectImageView.setBackgroundColor(Color.BLACK);
			projectImageView.setScaleType(ScaleType.FIT_XY);
			bm=bitmap(mImageNames.get(position));
			projectImageView.setImageBitmap(bm);
			projectImageView.setLayoutParams(new Gallery.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

			/*projectImageView.setLayoutParams(new Gallery.LayoutParams(320,380));*/
			return projectImageView;


		}
		
		private Bitmap bitmap(String name)
		{
			final Bitmap bitmap=BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(name, "drawable", getPackageName()));
			return bitmap;
		}
	}// end of inner class
		
		public static class MyRunnable implements Runnable {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				/*if(dialog!=null)
				{
					dialog.dismiss();
					dialog=null;
				}*/

			}

		}
		
		@Override
		protected void onDestroy() {
			// TODO Auto-generated method stub
			images =null;
			more =null;
			media=null;
			gallery=null;
			super.onDestroy();
			System.gc();


		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub

			if(homeImgButton.getId()==v.getId())
			{

				Intent intent=new Intent(getApplicationContext(),Home.class);
				startActivity(intent);
				overridePendingTransition(0, 0);
				this.finish();

			}
			if(porjectImageButton.getId()==v.getId())
			{

				Intent intent=new Intent(getApplicationContext(),GalleryScrollView.class);
				startActivity(intent);
				overridePendingTransition(0, 0);
				this.finish();

			}
			if(contactImageButton.getId()==v.getId())
			{

				Intent intent=new Intent(getApplicationContext(),ContactUs.class);
				startActivity(intent);
				overridePendingTransition(0, 0);
				this.finish();

			}
		}
		
	


}
