package com.phonethics.ariisto.cloud;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class InitImages {

	//Gallery image names
	private ArrayList<String> image_names=new ArrayList<String>();
	
	//info image names
	private ArrayList<String> info_image_names=new ArrayList<String>();
	
	//info text of particular image
	private LinkedHashMap <String, String> info_text=new LinkedHashMap<String,String>();

	/*	private ArrayList <String,String> info_text=new ArrayList<String,String>();*/
	public int getCount()
	{
		return image_names.size();

	}
	public void setImages(String image_name)
	{
		image_names.add(image_name);
	}
	public ArrayList<String> getImageArrayList()
	{
		return image_names;

	}
	public String getImageName(int position)
	{
		return image_names.get(position);

	}
	public void clearImages()
	{
		image_names.clear();
	}
	
	public void setInfoImages(String image_name)
	{
		info_image_names.add(image_name);
	}
	public String getInfoImageName(int position)
	{
		return info_image_names.get(position);

	}
	public void clearInfoImages()
	{
		info_image_names.clear();
	}
	
	public void setInfoText(String image_name,String desc)
	{
		info_text.put(image_name, desc);
	}
	public String getInfoText(String key)
	{
		return (String)info_text.get(key);

	}
	public void clearInfoText()
	{
		info_text.clear();
	}

}
