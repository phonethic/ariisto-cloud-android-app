package com.phonethics.ariisto.cloud;

import java.util.ArrayList;

import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;



public class AriistoLinkParser extends DefaultHandler {


	private Project tempProj;
	private String tempVal;
	String name;
	/*private boolean iphoneLink;
	private boolean ipadLink;*/
	private boolean androidLink;
	private boolean inside_charMethod;
	private int i=0;
	private boolean androidName;
	boolean log=false;

	/*public ExampleHandler(){
		projects = new ArrayList<Project>();
	}

	public List<Project> getProjects(){
		return projects;
	}*/

	private ArrayList<Project> projects;


	public ArrayList<Project> getParsedData(){
		return projects;
	}
	public void startDocument() throws SAXException{

		if(log)
		{
		Log.d("Document", "Inside XML Document");
		}
		this.projects = new ArrayList<Project>();
		tempProj=new Project();
	}
	public void endDocument() throws SAXException {
		// Nothing to do
	}
	@Override
	public void startElement(String uri, String localName, String qName,org.xml.sax.Attributes atts) throws SAXException {
		// TODO Auto-generated method stub
		super.startElement(uri, localName, qName, atts);

		tempVal = "";
		name ="";

		if(qName.equalsIgnoreCase("project")){
			tempProj = new Project();
			name = atts.getValue("name");
			tempProj.setName(name);
			i=i+1;
		}
		else if(qName.equalsIgnoreCase("android_link")){
			this.androidLink = true;
			this.inside_charMethod = false;

		}

	}


	public void characters(char[] ch, int start, int length)
			throws SAXException {
		if(this.androidLink){
			this.inside_charMethod = true;
			if(log)
			{
			Log.d("Length","Length "+length);
			}
			tempProj.setAndroidLink(new String(ch, start, length));
		}
		if(this.androidName)
		{
			this.androidName=true;
			tempProj.setName(new String(ch,start,length));
		}
		
	}
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if(log)
		{
		Log.d("endElement","Called");
		}
		if (qName.equalsIgnoreCase("project")) {
			// add it to the list
			this.androidName=false;
			projects.add(tempProj);
		} else if (qName.equalsIgnoreCase("android_link")) {
			/*tempProj.setAndroidLink(tempVal);*/
			this.androidLink = false;
			if(inside_charMethod == false){
				tempProj.setAndroidLink("null");
			}


		} 
	}

}
