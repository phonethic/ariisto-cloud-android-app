package com.phonethics.ariisto.cloud;





import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ViewSwitcher;

public class SpecialFeatures extends Activity {

	RelativeLayout 			nextLayout;
	ViewSwitcher			viewSwitch;
	ImageView				imgScreen, imgInfoIcon,innerInfo;
	Intent					intent;
	Context					context;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_special_features);

		context		= this;
		imgScreen	= (ImageView) findViewById(R.id.imageSpecialFeature);
		imgInfoIcon	= (ImageView) findViewById(R.id.imageInfoIcon);
		innerInfo=(ImageView)findViewById(R.id.imageInfo);
		viewSwitch	= (ViewSwitcher) findViewById(R.id.viewSwitcher);
		intent		= new Intent(context, FeturesListContent.class);
		nextLayout	= (RelativeLayout) findViewById(R.id.nextLayout);

		imgScreen.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				viewSwitch.showNext();


			}
		});
		imgInfoIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*viewSwitch.showPrevious();*/
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);

			}
		});
		innerInfo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(intent);
				overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(nextLayout.isShown()){
			viewSwitch.showPrevious();
			/*overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);*/
		}else{
			super.onBackPressed();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			this.finish();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_special_features, menu);
		return false;
	}
}
