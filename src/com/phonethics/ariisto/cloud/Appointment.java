

package com.phonethics.ariisto.cloud;





import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Appointment extends Activity implements OnClickListener {
	
	Button send;
	EditText name;
	EditText ph;
	EditText emailid;
	Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);
        
        send=(Button)findViewById(R.id.btn_Send);
		name=(EditText)findViewById(R.id.nameOfCust);
		ph=(EditText)findViewById(R.id.phoneOfCust);
		emailid=(EditText)findViewById(R.id.emaiId);

		context=this;
		send.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_appointment, menu);
        return false;
    }
    
    @Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
    	boolean flag=true;
    	
		if((name.getText().toString().equals("")))
		{
			AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
			alertDialog.setIcon(R.drawable.ariistologo);
			alertDialog.setTitle(" ");
			alertDialog.setCancelable(true);
			alertDialog.setMessage("Please Enter your Name");
			alertDialog.setNegativeButton("OK",new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});

			AlertDialog alert=alertDialog.create();
			alert.show();
		}
		else if(emailid.getText().toString().equals(""))
		{
			AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
			alertDialog.setIcon(R.drawable.ariistologo);
			alertDialog.setTitle(" ");
			alertDialog.setCancelable(true);
			alertDialog.setMessage("Please Enter your email-id");
			alertDialog.setNegativeButton("OK",new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});

			AlertDialog alert=alertDialog.create();
			alert.show();
		}

		else if(!isValidEmail(emailid.getText()))
		{
			
				flag=false;
				AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
				alertDialog.setIcon(R.drawable.ariistologo);
				alertDialog.setTitle(" ");
				alertDialog.setCancelable(true);
				alertDialog.setMessage("Please Enter your email-id Eg. abc@gmail.com");
				alertDialog.setNegativeButton("OK",new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});

				AlertDialog alert=alertDialog.create();
				alert.show();
			
		}
		else if(ph.getText().toString().equals(""))
		{

			AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
			alertDialog.setIcon(R.drawable.ariistologo);
			alertDialog.setTitle(" ");
			alertDialog.setCancelable(true);

			alertDialog.setMessage("Please Enter your Phone number");
			alertDialog.setNegativeButton("OK",new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});

			AlertDialog alert=alertDialog.create();
			alert.show();

		}
		else if(ph.getText().length()<10)
		{
		AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
		alertDialog.setIcon(R.drawable.ariistologo);
		alertDialog.setTitle(" ");
		alertDialog.setCancelable(true);

		alertDialog.setMessage("Please Enter correct Phone number");
		alertDialog.setNegativeButton("OK",new DialogInterface.OnClickListener() {

		@Override
		public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
		dialog.cancel();
		}
		});

		AlertDialog alert=alertDialog.create();
		alert.show();

		}

		
		else
		{

			Intent email=new Intent(Intent.ACTION_SEND);
			email.setType("plain/text");
			email.putExtra("android.intent.extra.EMAIL",new String[]{getResources().getString(R.string.emailId)});
			/*email.putExtra("android.intent.extra.EMAIL",new String[]{name.getText().toString()});*/
			email.putExtra("android.intent.extra.SUBJECT", "Appoinment");
			email.putExtra("android.intent.extra.TEXT",name.getText().toString()+"\n"+ph.getText().toString()+"\n"+emailid.getText().toString());
			email.setType("text/plain");
			startActivity(Intent.createChooser(email, "Send mail..."));
			
	/*		
			
			Intent email=new Intent(Intent.ACTION_SEND);
			email.setType("plain/text");
			email.putExtra(android.content.Intent.EXTRA_EMAIL,getResources().getString(R.string.emailId));
			email.putExtra("android.intent.extra.EMAIL",new String[]{getResources().getString(R.string.emailId)});
			email.putExtra("android.intent.extra.SUBJECT", "Subject");
			email.putExtra("android.intent.extra.TEXT", "Enter your text here");
			email.setType("text/plain");
			startActivity(Intent.createChooser(email, "Send mail..."));*/
		}
	}
    
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}
	
}
