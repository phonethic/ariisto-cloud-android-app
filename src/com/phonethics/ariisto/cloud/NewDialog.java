package com.phonethics.ariisto.cloud;



import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class NewDialog extends Activity {

	Context context;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_dialog);
        context = this;
        
        Button okButton 			= (Button) findViewById(R.id.button_ok);
		Button cancelButton 		= (Button) findViewById(R.id.button_cancel);
		
		okButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				File externalFile = new File(Environment.getExternalStorageDirectory(),"downloadedfile.pdf");
				externalFile.delete();
				finish();
				
			}
		});
		cancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				/*int pid = android.os.Process.myPid();
				   android.os.Process.killProcess(pid);*/

			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_new_dialog, menu);
        return true;
    }
}
