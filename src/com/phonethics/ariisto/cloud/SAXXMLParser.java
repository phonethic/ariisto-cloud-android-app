package com.phonethics.ariisto.cloud;

import java.io.InputStream;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import android.util.Log;

public class SAXXMLParser {
	public static ArrayList<Project> parse(InputStream is) {
		 ArrayList<Project> projects = null;
		 try {
			
			 
	            // create a XMLReader from SAXParser
	            XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser()
	                    .getXMLReader();
	            // create a ExampleHandler
	            AriistoLinkParser saxHandler = new AriistoLinkParser();
	            // store handler in XMLReader
	            xmlReader.setContentHandler(saxHandler);
	            // the process starts
	            xmlReader.parse(new InputSource(is));
	            // get the Project list`
	            /*projects = saxHandler.getParsedData();*/
	            
	            projects=saxHandler.getParsedData();
	            
	 
	        } catch (Exception ex) {
	            Log.d("XML", "ExampleParser: parse() failed");
	        }
		 return projects;
	 }
}
