package com.phonethics.ariisto.cloud;





import java.util.ArrayList;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class MoreDetails extends FragmentActivity implements OnClickListener, AnimationListener{

	Intent intent;
	ImageView dwnimg;
	ImageView cimg;

	ImageView moreImg;
	ImageView moreImageUp;

	ImageView backImage;

	Animation animate;
	Animation anim_img;

	ArrayList<String> control_img = new ArrayList<String>();
	ArrayList<String>control_img2 = new ArrayList<String>();
	ArrayList<String>control_img3 = new ArrayList<String>();
	ArrayList<ArrayList<String>> holder = new ArrayList<ArrayList<String>>();
	ArrayList<ArrayList<String>> classholder =new ArrayList<ArrayList<String>>();



	/*
	 * Grid Arrow Buttons
	 */
	ImageView controller,controllerDown;

	ArrayList<String> classListpage1 = new ArrayList<String>();
	ArrayList<String> classListpage2 = new ArrayList<String>();
	ArrayList<String> classListpage3 = new ArrayList<String>();
	float gridHeight;
	LayoutParams imageParam;
	int height;
	float animPos;
	boolean flag=true;
	int backCounter;

	MediaPlayer media;
	MediaPlayer dropDownButton;

	int cnt;

	TranslateAnimation  tc;
	TranslateAnimation  taSlide,taUpSlide;

	LayoutParams lp;
	LayoutParams linearParams;
	LayoutParams gridParams;
	GridView gridView;

	View grid;

	ImageView header;

	RelativeLayout rel;
	RelativeLayout RContainer;
	DisplayMetrics displaymetrics;

	RelativeLayout relHeader;

	/*
	 * GridView with Paging
	 */
	LinearLayout pagerdots;
	View pagerGroup;
	ViewPager mPager;
	TextView txtDots[];			//dots i.e pager indicator

	int screenHeight,screenWidth;
	static int calcHt;
	int vertSpacing,horSpacing;

	ImageView oneMore,downMore;
	LayoutParams oneMoreParam;
	Typeface tf;
	Context context;

	int arrowHeight;
	private LayoutParams downarrow_imageParam;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_more_details);
		
		context=this;
		tf = Typeface.createFromAsset(getResources().getAssets(),"fonts/bookman.ttf");
		
		backCounter=0;
		cnt=0;
		displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		screenHeight = displaymetrics.heightPixels;
		screenWidth = displaymetrics.widthPixels;
		backImage=(ImageView)findViewById(R.id.imageView_building);

		/*
		 * Getting Height of Header.
		 */
		BitmapDrawable bd=(BitmapDrawable) getApplicationContext().getResources().getDrawable(R.drawable.header_with_logo);
		height=bd.getIntrinsicHeight();

		/*
		 * Sound for Drawer
		 */
		media=MediaPlayer.create(getApplicationContext(),R.raw.folding_door_open);
		dropDownButton=MediaPlayer.create(getApplicationContext(),R.raw.folding_door_open);

		BitmapDrawable bdArrow=(BitmapDrawable) getApplicationContext().getResources().getDrawable(R.drawable.down);
		arrowHeight=bdArrow.getIntrinsicHeight();

		relHeader=(RelativeLayout)findViewById(R.id.relHeader);

		oneMore=new ImageView(getApplicationContext());
		downMore=new ImageView(getApplicationContext());

		oneMore.setImageResource(R.drawable.arrow_up);
		downMore.setImageResource(R.drawable.down);

		RContainer=new RelativeLayout(getApplicationContext()); //Creating Dynamic Relative Layout to add Grid and Buttons


		moreImg=(ImageView)findViewById(R.id.imageView_downArrow_);
		moreImageUp=(ImageView)findViewById(R.id.moreImageUp);


		dwnimg=new ImageView(getApplicationContext());
		dwnimg.setImageResource(R.drawable.arrow_up);


		cimg= new ImageView(getApplicationContext());
		cimg.setImageResource(R.drawable.down);
		cimg.setVisibility(View.INVISIBLE);
		/*intent  = new Intent();
		intent.setClass(this,Control_tools.class);*/
		/*cimg.setImageResource(R.drawable.down);*/
		/*	startActivityForResult(intent,1);*/
		dwnimg.setVisibility(View.VISIBLE);



 
		classListpage1.add("Location");
		classListpage1.add("GoogleMap");
		classListpage1.add("Elevetion");

		classListpage1.add("FloorPlan");
		classListpage1.add("UnitPlan");
		classListpage1.add("Amenities");

		classListpage2.add("GalleryActivity");
		classListpage2.add("Pdf");
		classListpage2.add("Appointment");
		
		classListpage2.add("SpecialFeatures");
		classListpage2.add("Credits");
		classListpage2.add("InnerContactUs");
		
		classListpage3.add("AboutCloud");


		control_img.add("location_icon");
		control_img.add("googlemap_icon");
		control_img.add("elevation_icon");
		
		
		control_img.add("floorplan_icon");
		control_img.add("units_icon");
		control_img.add("amenities_icon");

		control_img2.add("gallery_icon");
		control_img2.add("pdf_icon");
		control_img2.add("appointments_icon");
		
		control_img2.add("specialfeature_icon");
		control_img2.add("credits_icon");
		control_img2.add("contactus_icon");
		
		control_img3.add("aboutcloud_icon");
		/*
		 * Adding Images to Populate GridView
		 */
		holder.add(control_img);
		holder.add(control_img2);
		holder.add(control_img3);

		/*
		 * Adding Class 
		 */
		classholder.add(classListpage1);
		classholder.add(classListpage2);
		classholder.add(classListpage3);


		txtDots=new TextView[holder.size()];

		rel=(RelativeLayout)findViewById(R.id.moreAct);

		LayoutInflater inflator = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


		/*
		 * Inflating GridView  Pager
		 */
		pagerGroup=inflator.inflate(R.layout.pagergrid, null);
		mPager=(ViewPager)pagerGroup.findViewById(R.id.vpGrid);
		pagerdots=(LinearLayout)pagerGroup.findViewById(R.id.header);
		controller=(ImageView)pagerGroup.findViewById(R.id.controlGrid);
		controllerDown=(ImageView)pagerGroup.findViewById(R.id.controlGridDown);

		//Creating Dots for Pager
				for(int i=0;i<holder.size();i++)
				{
					txtDots[i]=new TextView(context);
					txtDots[i].setText(".");
					txtDots[i].setTypeface(tf);
					txtDots[i].setTextSize(45);
					
					pagerdots.addView(txtDots[i]);
				}
				
				txtDots[0].setTextColor(Color.WHITE);

		/*		LayoutParams pagerdotsParam = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.WRAP_CONTENT);

		pagerdotsParam.addRule(RelativeLayout.ABOVE,mPager.getId());
		pagerdots.setGravity(Gravity.CENTER_HORIZONTAL);
		pagerdots.setLayoutParams(pagerdotsParam);*/

		View headerStrip=inflator.inflate(R.layout.header_layout,null);


		header=(ImageView)headerStrip.findViewById(R.id.ariistoHeader);



		inflator.inflate(R.layout.gridview_up_arrow_button,null);

		//Inflating Gridveiw to use in activity
		/*	gridView=(GridView)grid.findViewById(R.id.inside_gridView);*/

		calcHt= (screenHeight-(height*4));

		linearParams=new LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,calcHt);


		lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
		lp.height=calcHt+arrowHeight;

		RContainer.setLayoutParams(linearParams);

		gridParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, calcHt);


		taSlide=new TranslateAnimation(0, 0, -calcHt-height, 0);

		taSlide.setDuration(1950);
		taSlide.setFillAfter(true);
		taSlide.setAnimationListener(this);


		/*
		 * Setting Images To gridview
		 */
		mPager.setAdapter(new MyAdapter(getSupportFragmentManager(), this, holder,classholder));

		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				/*				txt[arg0].setTextColor(Color.WHITE);*/


				for(int i=0;i<txtDots.length;i++)
				{
					if(i==arg0)
					{
						txtDots[i].setTextColor(Color.WHITE);
					}
					else
					{
						txtDots[i].setTextColor(Color.GRAY);
					}
				}



			}

			@Override
			public void onPageScrolled(int position, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int state) {
				// TODO Auto-generated method stub


			}
		});


		imageParam= new LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		downarrow_imageParam= new LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

		oneMoreParam=new LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

		imageParam.addRule(RelativeLayout.ALIGN_BOTTOM,mPager.getId());

		imageParam.setMargins(0, 0, 0, 0);
		/*downarrow_imageParam.setMargins(15, calcHt+height+arrowHeight, 0, 0);*/
		/*	downarrow_imageParam.addRule(RelativeLayout.BELOW, RContainer.getId());*/
		/*imageParam.setMargins(1,0,0, 0);
		imageParam.setMargins(10, 0, 0, 0);*/

		oneMoreParam.addRule(RelativeLayout.BELOW,relHeader.getId());

		oneMoreParam.setMargins(16, 0, 0, 0);

		lp.setMargins(14, 0, 14, 20); //Setting GridView Left Top Right and Bottom.
		lp.addRule(RelativeLayout.BELOW,moreImg.getId());

		/*		gridParams.setMargins(14, (height), 14, 20);
		 */
		/*		gridView.getBackground().setAlpha(200);*/
		//gridView.setLayoutParams(gridParams);
		mPager.setLayoutParams(gridParams);


		/*dwnimg.setLayoutParams(imageParam);*/
		dwnimg.setLayoutParams(imageParam);
		cimg.setLayoutParams(imageParam);


		((ViewGroup)mPager.getParent()).removeView(mPager);
		((ViewGroup)pagerdots.getParent()).removeView(pagerdots);
		((ViewGroup)controller.getParent()).removeView(controller);
		((ViewGroup)controllerDown.getParent()).removeView(controllerDown);

		RContainer.addView(mPager);
		RContainer.addView(pagerdots);
		RContainer.addView(controller);
		RContainer.addView(controllerDown);




		rel.addView(RContainer,lp);


		flag=false;

		relHeader.bringToFront();

		RContainer.startAnimation(taSlide);




		controller.setOnClickListener(new Cimage());

		moreImg.setOnClickListener(this);
		/*	gridView.setOnItemClickListener(this);*/
		oneMore.setOnClickListener(new DwnImage());
		oneMore.setOnTouchListener(new DwnImage() );
	}

	class Cimage implements OnClickListener,OnTouchListener
	{

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			taUpSlide=new TranslateAnimation(0, 0,0,-screenHeight+(height*4));
			taUpSlide.setDuration(1950);
			taUpSlide.setFillAfter(true);
			RContainer.startAnimation(taUpSlide);
			taUpSlide.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					moreImg.setVisibility(View.GONE);
					downMore.setVisibility(View.GONE);
					media.start();
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					RContainer.setVisibility(View.GONE);
					mPager.setVisibility(View.GONE);

					if(oneMore.getParent()==null)
					{
						rel.addView(oneMore,oneMoreParam);
					}

			
					oneMore.postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							oneMore.setVisibility(View.VISIBLE);
						}
					}, 1500);


					applyRotation(0, 90,controllerDown,controller);
				}
			});

		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub


			return false;
		}

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		if((keyCode==event.KEYCODE_BACK))
		{

			backCounter++;
		}

		return super.onKeyDown(keyCode, event);
	}

	class DwnImage implements OnClickListener,OnTouchListener
	{

		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub

			RContainer.setVisibility(View.VISIBLE);
			mPager.setVisibility(View.VISIBLE);
			RContainer.startAnimation(taSlide);
			/*dwnimg.startAnimation(taSlide);*/
		}

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			/*	RContainer.setVisibility(View.VISIBLE);
			gridView.setVisibility(View.VISIBLE);
			RContainer.startAnimation(taSlide);
			return true;*/
			return false;
		}

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_more_details, menu);
		return false;
	}

	@Override
	public void onClick(View v) {

		if(v.getId()==moreImg.getId())
		{
			RContainer.setVisibility(View.VISIBLE);
			mPager.setVisibility(View.VISIBLE);
			moreImageUp.setVisibility(View.GONE);
			moreImg.setVisibility(View.GONE);
			RContainer.startAnimation(taSlide);



		}
		if(v.getId()==moreImageUp.getId())
		{

		}
	}





	private void applyRotation(float start, float end,ImageView image1,ImageView image2) {
		// Find the center of image
		final float centerX = image1.getWidth() / 2.0f;
		final float centerY = image1.getHeight() / 2.0f;
		// Create a new 3D rotation with the supplied parameter
		// The animation listener is used to trigger the next animation
		final Flip3dAnimation rotation =
				new Flip3dAnimation(start, end, centerX, centerY);
		rotation.setDuration(550);
		rotation.setFillAfter(true);
		rotation.setInterpolator(new AccelerateInterpolator());
		rotation.setAnimationListener(new DisplayNextView(true, image1, image2));
		image1.startAnimation(rotation);
		/*image1.startAnimation(rotation);
		image1.setVisibility(View.GONE);
		 *//*		cimg.setVisibility(View.VISIBLE);*/



	}





	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		intent=null;
		dwnimg=null;
		cimg=null;
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent=new Intent(getApplicationContext(),Projects.class);
		startActivity(intent);
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub
		/*	imageParam.setMargins(14,gridHeight+height, 0, 0);*/


	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		/*moreImg.setVisibility(View.GONE);*/

		/*		animate=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
		animate.setDuration(800);
		RContainer.startAnimation(animate);*/

		/*		relHeader.bringToFront();*/

		RContainer.setBackgroundColor(Color.TRANSPARENT);
		moreImg.setVisibility(View.GONE);
		/*		gridView.getBackground().setAlpha(200);*/
		/*		cimg.setVisibility(View.GONE);*/
		oneMore.setVisibility(View.GONE);
		/*	applyRotation(0, 90,dwnimg,cimg);*/
		applyRotation(0, 90,controller,controllerDown);
		/*		cnt++;*/


		media.start();
		/*moreImg.setVisibility(View.VISIBLE);*/
	}




	/*
	 * ViewPager Adapter
	 */


	public  class MyAdapter extends FragmentStatePagerAdapter
	{

		Context context;
		ArrayList<ArrayList<String>> holder=null;
		ArrayList<ArrayList<String>> classholder=null;
		public MyAdapter(FragmentManager fm,Context context,ArrayList<ArrayList<String>>holder,ArrayList<ArrayList<String>> classholder) {
			super(fm);
			// TODO Auto-generated constructor stub
			this.context=context;
			this.holder=holder;
			this.classholder=classholder;
			/*for(int i=0;i<url.size();i++)
			{
				Log.i("Link:"," "+url.get(i));
			}*/
		}

		@Override
		public Fragment getItem(int position) {
			// TODO Auto-generated method stub
		
			
			
			return new ImageGrid(context,holder.get(position),classholder.get(position));

		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return holder.size();
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			// TODO Auto-generated method stub

			FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();

			super.destroyItem(container, position, object);
		}



	}


}
