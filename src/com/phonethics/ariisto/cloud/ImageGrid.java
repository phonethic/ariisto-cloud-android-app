package com.phonethics.ariisto.cloud;

import java.util.ArrayList;



import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;



public class ImageGrid  extends Fragment {

	Context context;
	ArrayList<String>gridIcons;
	ArrayList<String> classList = new ArrayList<String>();
	public ImageGrid()
	{

	}
	public ImageGrid(Context context,ArrayList<String> grid,ArrayList<String> classList)
	{
		this.context=context;
		this.gridIcons=grid;
		this.classList=classList;
		for(int i=0;i<grid.size();i++)
		{
			Log.i("Came In Fragment: ", " "+gridIcons.get(i));
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRetainInstance(true);

	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view=inflater.inflate(R.layout.customgrid, container,false);

		GridView grid=(GridView)view.findViewById(R.id.gridView1);
		grid.setAdapter(new GridAdapter(context, gridIcons));

		grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = null;
				try {
					
					Class<?> target =Class.forName(context.getPackageName()+"."+classList.get(position));
					intent = new Intent(context,target);
					startActivity(intent);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*startActivity(intent);*/
				getActivity().overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
				}
		});

		return view;
	}







	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		if (outState.isEmpty()) {
			
			outState.putStringArrayList("ImageName", gridIcons );
			outState.putStringArrayList("className", classList );
		}
		super.onSaveInstanceState(outState);
	}




	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onActivityCreated(savedInstanceState);
		if(savedInstanceState != null){
			gridIcons = savedInstanceState.getStringArrayList("ImageName");
			classList = savedInstanceState.getStringArrayList("className");
		}
	}




	public class GridAdapter extends BaseAdapter {
		private Context context=null;
		private ArrayList<String> control_img;

		public GridAdapter(Context context,ArrayList<String> control_img) {
			this.control_img=control_img;
			Log.i("Inside Grid"," ");
			this.context = context;


		}

		@Override
		public int getCount() {

			return control_img.size();

		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View grid;
			if(convertView == null){

				grid = new View(context);

				grid = inflator.inflate(R.layout.gridicons,null);


			}
			else{
				grid = (View) convertView;
			}
			ImageView img = (ImageView) grid.findViewById(R.id.gridIcon);
			Log.i("here: ", " "+control_img.get(position));
			img.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+control_img.get(position),"drawable",context.getPackageName())));
			/*			img.setImageResource(R.drawable.amenities);*/
			img.setScaleType(ScaleType.FIT_XY);
			return grid;
		}



	}






}
