package com.phonethics.ariisto.cloud;



import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class FloorPlan extends Activity implements OnClickListener {
	
	private ImageView floorImage;
	private ImageView img2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor_plan);
        
        /*floorImage=(ImageView)findViewById(R.id.image_FloorPlan);
		floorImage.setOnClickListener(this);*/
        
        TouchImageView locationImg = (TouchImageView) findViewById(R.id.image_floor_plan);
        locationImg.setMaxZoom(3f);

    }

    @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
    	super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_floor_plan, menu);
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Bitmap bm=BitmapFactory.decodeResource(getResources(), R.drawable.floorplan);
		SandboxView1 img=new SandboxView1(getApplicationContext());
		img.setImageBitmap(bm);
		img.setMaxZoom(5);
		setContentView(img);
	}

}
