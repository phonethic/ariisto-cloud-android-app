package com.phonethics.ariisto.cloud;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class ControlToolsGrid extends Activity implements OnClickListener,AnimationListener {
	ArrayList<String> control_img 		= new ArrayList<String>();
	ArrayList<Class<?>> classList 		= new ArrayList<Class<?>>();
	Context 			context;
	RelativeLayout 		rel;
	ImageView 			upArrowImage;
	Animation 			animate;
	int backCounter;

	private boolean isFirstImage = true;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_control_tools_grid);

		context=getApplicationContext();
		backCounter=0;

		classList.add(Location.class);
		classList.add(Location.class);
		classList.add(Elevetion.class);

		classList.add(FloorPlan.class);
		classList.add(UnitPlan.class);
		classList.add(Amenities.class);

		classList.add(GalleryActivity.class);
		classList.add(Pdf.class);
		classList.add(Appointment.class);

		classList.add(SpecialFeatures.class);
		classList.add(Credits.class);
		classList.add(Credits.class);

		classList.add(AboutCloud.class);

		control_img.add("location_icon");
		control_img.add("googlemap_icon");

		control_img.add("elevation_icon");
		control_img.add("floorplan_icon");

		control_img.add("units_icon");
		control_img.add("amenities_icon");

		control_img.add("gallery_icon");
		control_img.add("pdf_icon");

		control_img.add("appointments_icon");
		control_img.add("specialfeature_icon");

		control_img.add("credits_icon");
		control_img.add("contactus_icon");

		control_img.add("aboutcloud_icon");


		BitmapDrawable 	bd		= 	(BitmapDrawable) getApplicationContext().getResources().getDrawable(R.drawable.header_with_logo);
		final int 		height	= 	bd.getIntrinsicHeight();
		int 			width	=	bd.getIntrinsicWidth();

		//Height and width of header image
		BitmapDrawable bd1=(BitmapDrawable) getApplicationContext().getResources().getDrawable(R.drawable.down);
		int arrowHeight=bd1.getIntrinsicHeight();
		int arrowwidth=bd1.getIntrinsicWidth();

		
		rel=(RelativeLayout)findViewById(R.id.parentRel);

		/*---------------------------------*/

		animate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
		animate.setAnimationListener(this);

		/*---------------------------------*/
		
		
		//get Height of status bar
		final int statusBarHeight =(int) Math.ceil(25 * context.getResources().getDisplayMetrics().density);

		int gridHeight=0;
		/*---------------------------------*/

		LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View grid2;

		//Getting Gridview and setting its position	
		final GridView menuGrid=(GridView)findViewById(R.id.inside_gridView);

		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(14, (height), 14, 20);

		menuGrid.setLayoutParams(params); //setting margins to grid

		grid2=new View(context);
		grid2=inflator.inflate(R.layout.gridview_up_arrow_button,null);

		final LayoutParams lp = new LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

		int bot=menuGrid.getHeight();
		
		menuGrid.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

			@Override
			public void onGlobalLayout() {
				// TODO Auto-generated method stub

				lp.setMargins(14, rel.getHeight(), 0, 0);
			}
		});
		
		upArrowImage=(ImageView)grid2.findViewById(R.id.imageview_up_arrow);

		((ViewGroup)upArrowImage.getParent()).removeView(upArrowImage);


		rel.addView(upArrowImage,lp);

		final ImageView downArrowImage=new ImageView(getApplicationContext());

		downArrowImage.setImageResource(R.drawable.down);
		downArrowImage.setVisibility(View.GONE);
		
		final LayoutParams lpp = new LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		menuGrid.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {


			@Override
			public void onGlobalLayout() {
				// TODO Auto-generated method stub
				int ht=(rel.getHeight()-height)+8;
				lpp.setMargins(14,ht, 0, 0);
				Log.d("Rel Height"+rel.getHeight(),"Height"+ht);

			}
		});
		downArrowImage.setLayoutParams(lpp);
		rel.addView(downArrowImage,lpp);
		
		upArrowImage.setImageResource(R.drawable.arrow_up);
		upArrowImage.setVisibility(View.VISIBLE);
		//-----------
		Animation anim=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
		anim.setAnimationListener(this);

		applyRotation(0, 90,upArrowImage,downArrowImage);
		isFirstImage = !isFirstImage;


		rel.startAnimation(anim);
		
		anim.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub

			}
		});
		
		upArrowImage.setOnClickListener(this);


		/*---------------------------------*/
		//Handling click on gridview

		GridView grid = (GridView) findViewById(R.id.inside_gridView);
		grid.getBackground().setAlpha(210);
		grid.setAdapter(new Ariisto_image_adapter(this,control_img));

		grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long arg3) {

				Intent intent =new Intent(getApplicationContext(),classList.get(position));

				startActivity(intent);
			}
		});
		
		

	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub


		if(v.getId()==R.id.imageview_up_arrow)
		{

			rel.startAnimation(animate);

		}


	}
	
	private void applyRotation(float start, float end,ImageView image1,ImageView image2) {
		// Find the center of image
		final float centerX = image1.getWidth() / 2.0f;
		final float centerY = image1.getHeight() / 2.0f;

		// Create a new 3D rotation with the supplied parameter
		// The animation listener is used to trigger the next animation
		final Flip3dAnimation rotation = new Flip3dAnimation(start, end, centerX, centerY);
		rotation.setDuration(500);
		rotation.setFillAfter(true);
		rotation.setInterpolator(new AccelerateInterpolator());
		rotation.setAnimationListener(new DisplayNextView(isFirstImage, image1, image2));

		if (isFirstImage)
		{
			image1.startAnimation(rotation);
		} else {
			image2.startAnimation(rotation);
		}


	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		if((keyCode==event.KEYCODE_BACK))
		{

			backCounter++;
		}

		return super.onKeyDown(keyCode, event);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_control_tools_grid, menu);
		return true;
	}
	@Override
	public void finish() {
		// TODO Auto-generated method stub
		setResult(1);

		super.finish();

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub

		finish();
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		upArrowImage.setVisibility(View.GONE);

		if(backCounter>1)
		{
			this.finish();
		}

	}

}
