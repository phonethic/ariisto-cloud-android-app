package com.phonethics.ariisto.cloud;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.ToggleButton;

public class Elevetion extends Activity implements OnClickListener, OnTouchListener {
	private ToggleButton 	tog_button;
	ImageView 				imgDayNight;
  
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elevetion);
        
        tog_button 		= (ToggleButton) findViewById(R.id.button1);
		imgDayNight 	= (ImageView)	findViewById(R.id.imageDayNight);
		tog_button.setOnClickListener(this);

    }
	

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_elevetion, menu);
        return false;
    }
    
    @Override
	public void onClick(View v) {
		if(tog_button.isChecked() ){
			imgDayNight.setBackgroundResource(R.drawable.dayview);;			
		}else{
			imgDayNight.setBackgroundResource(R.drawable.nightview);
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}

}
