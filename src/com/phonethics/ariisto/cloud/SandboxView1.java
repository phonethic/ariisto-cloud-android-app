package com.phonethics.ariisto.cloud;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

public class SandboxView1 extends ImageView implements OnTouchListener  {
	/*private final Bitmap bitmap;
	*/
	private boolean isInitialized = false;
	private TouchManager touchManager = new TouchManager(2);
	private Vector2D vca = null;
	private Vector2D vcb = null;
	private Vector2D vpa = null;
	private Vector2D vpb = null;
	private Vector2D position = new Vector2D();
	private float scale = 1.f;
	ImageView view;
	ScaleGestureDetector mScaleDetector;
	Matrix matrix = new Matrix();
	// We can be in one of these 3 states
	static final int NONE = 0;
	static final int DRAG = 1;
	static final int ZOOM = 2;
	int mode = NONE;
	// Remember some things for zooming
	PointF last = new PointF();
	PointF start = new PointF();
	float minScale = 1f;
	float maxScale = 3f;
	float[] m;
	float redundantXSpace, redundantYSpace;
	Context context;

	float width, height;
	static final int CLICK = 3;
	float saveScale = 1f;
	float right, bottom, origWidth, origHeight, bmWidth, bmHeight;
	
	public SandboxView1(Context context) {
		super(context);
	    this.context = context;
	    mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
	    
		matrix.setTranslate(1f, 1f);
	    m = new float[9];
	    setImageMatrix(matrix);
	    setScaleType(ScaleType.MATRIX);
	
	
	    setOnTouchListener(new OnTouchListener() {
		 @Override
	        public boolean onTouch(View v, MotionEvent event) {
	            mScaleDetector.onTouchEvent(event);
	            matrix.getValues(m);
	            float x = m[Matrix.MTRANS_X];
	            float y = m[Matrix.MTRANS_Y];
	            PointF curr = new PointF(event.getX(), event.getY());
	            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    last.set(event.getX(), event.getY());
                    start.set(last);
                    mode = DRAG;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) {
                        float deltaX = curr.x - last.x;
                        float deltaY = curr.y - last.y;
                        float scaleWidth = Math.round(origWidth * saveScale);
                        float scaleHeight = Math.round(origHeight * saveScale);
                        if (scaleWidth < width) {
                            deltaX = 0;
                            if (y + deltaY > 0)
                                deltaY = -y;
                            else if (y + deltaY < -bottom)
                                deltaY = -(y + bottom); 
                        } else if (scaleHeight < height) {
                            deltaY = 0;
                            if (x + deltaX > 0)
                                deltaX = -x;
                            else if (x + deltaX < -right)
                                deltaX = -(x + right);
                        } else {
                            if (x + deltaX > 0)
                                deltaX = -x;
                            else if (x + deltaX < -right)
                                deltaX = -(x + right);

                            if (y + deltaY > 0)
                                deltaY = -y;
                            else if (y + deltaY < -bottom)
                                deltaY = -(y + bottom);
                        }
                        matrix.postTranslate(deltaX, deltaY);
                        last.set(curr.x, curr.y);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    mode = NONE;
                    int xDiff = (int) Math.abs(curr.x - start.x);
                    int yDiff = (int) Math.abs(curr.y - start.y);
                    if (xDiff < CLICK && yDiff < CLICK)
                        performClick();
                    break;

                case MotionEvent.ACTION_POINTER_UP:
                    mode = NONE;
                    break;
	            }
	            setImageMatrix(matrix);
	            invalidate();
	            return true; // indicate event was handled
		    }

	    });
	 }
	public void setImageBitmap(Bitmap bm) { 
	    super.setImageBitmap(bm);
	    bmWidth = bm.getWidth();
	    bmHeight = bm.getHeight();
	}
	public void setMaxZoom(float x)
	{
	    maxScale = x;
	}
	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
	    @Override
	    public boolean onScaleBegin(ScaleGestureDetector detector) {
	        mode = ZOOM;
	        return true;
	    }

	    @Override
	    public boolean onScale(ScaleGestureDetector detector) {
	        float mScaleFactor = (float)Math.min(Math.max(0.5f, detector.getScaleFactor()), 2.4);
	        float origScale = saveScale;
	        saveScale *= mScaleFactor;
	        if (saveScale > maxScale) {
	            saveScale = maxScale;
	            mScaleFactor = maxScale / origScale;
	        } else if (saveScale < minScale) {
	            saveScale = minScale;
	            mScaleFactor = minScale / origScale;
	        }
	        right = width * saveScale - width - (2 * redundantXSpace * saveScale);
	        bottom = height * saveScale - height - (2 * redundantYSpace * saveScale);
	        if (origWidth * saveScale <= width || origHeight * saveScale <= height) {
	            matrix.postScale(mScaleFactor, mScaleFactor, width / 2, height / 2);
	            if (mScaleFactor < 1) {
	                matrix.getValues(m);
	                float x = m[Matrix.MTRANS_X];
	                float y = m[Matrix.MTRANS_Y];
	                if (mScaleFactor < 1) {
	                    if (Math.round(origWidth * saveScale) < width) {
	                        if (y < -bottom)
	                            matrix.postTranslate(0, -(y + bottom));
	                        else if (y > 0)
	                            matrix.postTranslate(0, -y);
	                    } else {
	                        if (x < -right) 
	                            matrix.postTranslate(-(x + right), 0);
	                        else if (x > 0) 
	                            matrix.postTranslate(-x, 0);
	                    }
	                }
	            }
	        } else {
	            matrix.postScale(mScaleFactor, mScaleFactor, detector.getFocusX(), detector.getFocusY());
	            matrix.getValues(m);
	            float x = m[Matrix.MTRANS_X];
	            float y = m[Matrix.MTRANS_Y];
	            if (mScaleFactor < 1) {
	                if (x < -right) 
	                    matrix.postTranslate(-(x + right), 0);
	                else if (x > 0) 
	                    matrix.postTranslate(-x, 0);
	                if (y < -bottom)
	                    matrix.postTranslate(0, -(y + bottom));
	                else if (y > 0)
	                    matrix.postTranslate(0, -y);
	            }
	        }
	        return true;

	    }
	}

	@Override
	protected void onMeasure (int widthMeasureSpec, int heightMeasureSpec)
	{
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	    width = MeasureSpec.getSize(widthMeasureSpec);
	    height = MeasureSpec.getSize(heightMeasureSpec);
	    //Fit to screen.
	    float scale;
	    float scaleX =  (float)width / (float)bmWidth;
	    float scaleY = (float)height / (float)bmHeight;
	    scale = Math.min(scaleX, scaleY);
	    matrix.setScale(scale, scale);
	    setImageMatrix(matrix);
	    saveScale = 1f;

	    // Center the image
	    redundantYSpace = (float)height - (scale * (float)bmHeight) ;
	    redundantXSpace = (float)width - (scale * (float)bmWidth);
	    redundantYSpace /= (float)2;
	    redundantXSpace /= (float)2;

	    matrix.postTranslate(redundantXSpace, redundantYSpace);

	    origWidth = width - 2 * redundantXSpace;
	    origHeight = height - 2 * redundantYSpace;
	    right = width * saveScale - width - (2 * redundantXSpace * saveScale);
	    bottom = height * saveScale - height - (2 * redundantYSpace * saveScale);
	    setImageMatrix(matrix);
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
	
	/*protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		if (!isInitialized) {
			int w = getWidth();
			int h = getHeight();
			position.set(w/2, h/2);
			isInitialized = true;
		}
		
	}


	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		
		vca = null;
		vcb = null;
		vpa = null;
		vpb = null;
		

		try {
			touchManager.update(event);

			if (touchManager.getPressCount() == 1) {
				vca = touchManager.getPoint(0);
				vpa = touchManager.getPreviousPoint(0);
				position.add(touchManager.moveDelta(0));
			}
			else {
				if (touchManager.getPressCount() == 2) {
					vca = touchManager.getPoint(0);
					vpa = touchManager.getPreviousPoint(0);
					vcb = touchManager.getPoint(1);
					vpb = touchManager.getPreviousPoint(1);

					Vector2D current = touchManager.getVector(0, 1);
					Vector2D previous = touchManager.getPreviousVector(0, 1);
					float currentDistance = current.getLength();
					float previousDistance = previous.getLength();

					if (previousDistance != 0) {
						scale *= currentDistance / previousDistance;
						if(scale <=1 ){
						scale = 1;
						scale *= currentDistance / previousDistance;
						scale = Math.max(MIN_ZOOM, Math.min(scale, MAX_ZOOM));	
						}
						if(scale >=3){
						scale = 3;
						scale *= currentDistance / previousDistance;
						
						}
						
					}

					
				}
			}

			invalidate();
		}
		catch(Throwable t) {
			// So lazy...
		

		
	}
		return true;
	}*/
}