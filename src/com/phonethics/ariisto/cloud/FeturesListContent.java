package com.phonethics.ariisto.cloud;





import java.util.ArrayList;


import android.R.color;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class FeturesListContent extends Activity {

	ImageView 				imgFeture;
	ListView 				listview;
	ArrayList<String> 				values;
	CustomListAdapter 	adapter;
	int tempPosition;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fetures_list_content);

		listview 	= (ListView) findViewById(R.id.listView1);
		values=new ArrayList<String>();
		values.add(" ESSENTIAL      ");
		values.add(" EMERGENCY     ");
		values.add(" ENTERTAINMENT  ");
		values.add(" VIDEO CALL     ");
		values.add(" DOCTORS     ");
		values.add(" SOCIETY      ");
		values.add(" FAMILY      ");
		values.add(" OTHERS      ");
		values.add(" WHAT'S NEW     ");


		imgFeture	= (ImageView) findViewById(R.id.imageFeatures);

		adapter 	= new CustomListAdapter(this, values);
		listview.setAdapter(adapter);
		adapter.toggleSelected(0);

		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v,
					int position, long id) {
				imgFeture.setBackgroundDrawable(getResources().getDrawable(getResources().getIdentifier("drawable/feature_"+position,"drawable",getPackageName())));
				adapter.toggleSelected(position);
				adapter.notifyDataSetChanged();

			}
		});
	}

	public class CustomListAdapter extends ArrayAdapter<String>
	{
		Activity context;
		ArrayList<String> row=null;
		LayoutInflater inflator=null;
		public ArrayList<Integer> selectedIds = new ArrayList<Integer>();
		
		
		public CustomListAdapter(Activity context,ArrayList<String> row)
		{
			super(context,R.layout.listrow,row);
			this.context=context;
			this.row=row;

		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			View rowView=convertView;

			if(convertView==null)
			{
				inflator					=	context.getLayoutInflater();
				final ViewHolder holder		=	new ViewHolder();
				rowView						=	inflator.inflate(R.layout.listrow, null);
				holder.rowText				=	(TextView) rowView.findViewById(R.id.rowListTextView);
				holder.layout 				=	(RelativeLayout) rowView.findViewById(R.id.text_relativelayout);
				

				rowView.setTag(R.id.rowListTextView, holder.rowText);
				rowView.setTag(R.id.text_relativelayout, holder.layout);
				rowView.setTag(holder);				



			}
			ViewHolder hold=(ViewHolder)rowView.getTag();
			hold.rowText.setText(row.get(position));
			if (selectedIds.contains(position)){
				  hold.layout.setBackgroundColor(Color.GRAY);
				  hold.rowText.setTextColor(Color.WHITE);
			  }else{
				  hold.layout.setBackgroundColor(Color.WHITE);
				  hold.rowText.setTextColor(Color.BLACK);
			  }
			
			
			return rowView;
		}
		
		
		public void toggleSelected(Integer position)
		{

			selectedIds.clear();
			selectedIds.add(position);
		}
		


	}

	static class ViewHolder
	{
		TextView rowText;
		RelativeLayout layout;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_fetures_list_content, menu);
		return false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}
}
