package com.phonethics.ariisto.cloud;





import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

public class Pdf extends Activity {

	private ImageView 				pdfimage;
	private ProgressDialog 			pDialog;
	public static final int 		progress_bar_type = 1; 
	private static String 			file_url;
	WebView 						webview;
	Intent 							intent1;
	NetworkCheck 					isConnected;
	Context							context;
	private volatile boolean running = true;
	int	count = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pdf);
		context			= 	this;
		intent1 		= 	new Intent(Intent.ACTION_VIEW);
		isConnected		=	new NetworkCheck(getApplicationContext());
		file_url 		= 	"http://ariisto.com/pdf/Ariisto%20Cloud%20Brochure.pdf";

		final File externalFile = new File(Environment.getExternalStorageDirectory(),"AriistoCloud.pdf");
		final Uri external 		= Uri.fromFile(externalFile);
		pdfimage 				= (ImageView)findViewById(R.id.imagePDF);

		//boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
		String state = Environment.getExternalStorageState();
		/*    if (Environment.MEDIA_MOUNTED.equals(state)) {*/
		Log.i("Media : ", " "+state);
		if(Environment.MEDIA_MOUNTED.equals(state))
		{

			
			pdfimage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					count = 0;
					if(externalFile.exists()){
						if((externalFile.length() / 1024) != 0){
							viewPdf(external);
						}else{
							externalFile.delete();
						}

					}
					else{
						if(isConnected.isNetworkAvailable()){
							new DownloadFileFromURL().execute(file_url);
						}else{
							Toast.makeText(getApplicationContext(), "Check your Internet Connection", Toast.LENGTH_SHORT).show();
						}
					}

				}
			});

			if(externalFile.exists()){
				if((externalFile.length() / 1024) != 0){
					viewPdf(external);
				}else{
					externalFile.delete();
					if(isConnected.isNetworkAvailable()){
						new DownloadFileFromURL().execute(file_url);
					}else{
						Toast.makeText(getApplicationContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
					}
				}
			}
			else{
				if(isConnected.isNetworkAvailable()){
					new DownloadFileFromURL().execute(file_url);
				}else{
					Toast.makeText(getApplicationContext(), "Check Internet Connection", Toast.LENGTH_SHORT).show();
				}
			}
		}
		else
		{
			Intent web=new Intent(Intent.ACTION_VIEW);
			web.setData(Uri.parse(file_url));
			startActivity(web);
		}
	}

	private void viewPdf(Uri file){
		Intent intent;
		intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(file, "application/pdf");
		try{
			startActivity(intent);
		}catch(ActivityNotFoundException e){
			AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
			builder.setTitle("No Application Found");
			builder.setMessage("Download from Android Market?");
			builder.setPositiveButton("Yes, Please", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

					Intent marketIntent = new Intent(Intent.ACTION_VIEW);
					marketIntent.setData(Uri.parse("market://details?id=com.adobe.reader"));
					startActivity(marketIntent);

				}
			});
			builder.setNegativeButton("No, Thanks", null);
			builder.create().show();

		}


	}
	/**
	 * Showing Dialog
	 */
	@Override
	protected Dialog onCreateDialog(int id)
	{
		switch (id)
		{
		case progress_bar_type:
			pDialog = new ProgressDialog(this);
			pDialog.setMessage("Downloading file. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setMax(100);
			pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.setCancelable(true);
			pDialog.show();

			/*pDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {

				@Override
				public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
					// TODO Auto-generated method stub
					if(keyCode == KeyEvent.KEYCODE_BACK){

						running = false;
						Intent intent = new Intent(context, NewDialog.class);
						startActivity(intent);
						AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
						alertDialog.setIcon(R.drawable.ic_launcher);
						alertDialog.setTitle("Ariisto");
						alertDialog.setMessage("Do you Want to Cancel the Download ?");
						alertDialog.setCancelable(true);

						alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub

								File externalFile = new File(Environment.getExternalStorageDirectory(),"downloadedfile.pdf");
								externalFile.delete();
								pDialog.dismiss();
								running = false;
								Log.d("External File", "DELETED");
								pDialog.setProgress(0);
								count = 2;


							}
						});
						alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								new DownloadFileFromURL().execute(file_url);
								running = true;
								count = 0;
							}
						});

						AlertDialog alert = alertDialog.create();
						if(count == 0){
							alert.show();
						}
						alert.show();
						return true;

					}
					return true;
					return false;

				}
			});*/

			pDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					Log.d("------------","**** pDIALOG OFF *****");
					Log.d("------------","**** running off *****");
					File externalFile = new File(Environment.getExternalStorageDirectory(),"AriistoCloud.pdf");
					externalFile.delete();
					running = false;
					externalFile = new File(Environment.getExternalStorageDirectory(),"AriistoCloud.pdf");
					if(!externalFile.exists()){
						Log.d("------------","**** No Such File *****");
					}
					/*	running = false;
					AlertDialog.Builder  alertDialog = new AlertDialog.Builder(context);
					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setTitle("Ariisto");
					alertDialog.setMessage("Download has been Interrupted. Do you want to Download it Again ?");
					alertDialog.setCancelable(false);
					alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							File externalFile = new File(Environment.getExternalStorageDirectory(),"downloadedfile.pdf");
							externalFile.delete();
							pDialog.dismiss();
							running = false;
							Log.d("External File", "DELETED");
							pDialog.setProgress(0);
							new DownloadFileFromURL().execute(file_url);
							running = true;

						}
					});
					alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							File externalFile = new File(Environment.getExternalStorageDirectory(),"downloadedfile.pdf");
							externalFile.delete();
							pDialog.dismiss();
							running = false;
							Log.d("External File", "DELETED");
							pDialog.setProgress(0);

						}
					});
					AlertDialog alert = alertDialog.create();
					alert.show();
					 */					


				}
			});

			return pDialog;
		default:
			return null;
		}
	}
	class DownloadFileFromURL extends AsyncTask<String, String, String> 
	{

		/**
		 * Before starting background thread
		 * Show Progress Bar Dialog
		 * */




		@SuppressWarnings("deprecation")
		protected void onPreExecute()
		{
			super.onPreExecute();
			showDialog(progress_bar_type);
			publishProgress(""+(int)(0));
			running = true;
		}

		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub

			Log.d("------------","iNSIDE ON CANCELLED METHOD");

			super.onCancelled();
		}



		@Override
		protected String doInBackground(String... file_url) 
		{
			// TODO Auto-generated method stub
			int count;
			try
			{
				URL url = new URL(file_url[0]);
				URLConnection connection = url.openConnection();
				connection.connect();
				// getting file length
				int lenghtOfFile = connection.getContentLength();

				// input stream to read file - with 8k buffer
				InputStream input = new BufferedInputStream(url.openStream(), 8192);

				// Output stream to write file
				FileOutputStream output = new FileOutputStream("/sdcard/AriistoCloud.pdf");


				byte data[] = new byte[1024];

				long total = 0;

				while (   ((count = input.read(data)) != -1) && (running == true)  ) 
				{


					/*if(!pDialog.isShowing()){
						File externalFile = new File(Environment.getExternalStorageDirectory(),"downloadedfile.pdf");

						if((externalFile.length()) < lenghtOfFile){

							Log.d("------------","**** pDIALOG OFF *****");
							externalFile.delete();
							pDialog.dismiss();
							Log.d("External File", "DELETED");
						}


						total = 0;

						publishProgress(""+(int)(0));

						break;
					}
					 */

					total += count;

					// publishing the progress....
					// After this onProgressUpdate will be called
					publishProgress(""+(int)((total*100)/lenghtOfFile));

					// writing data to file
					output.write(data, 0, count);
				}

				output.flush();

				// closing streams
				output.close();
				input.close();
			}
			catch (Exception e)
			{
				Log.e("Error: ", " "+e.getMessage());
			}
			return null;
		}

		protected void onProgressUpdate(String... progress) 
		{
			// setting progress percentage
			pDialog.setProgress(Integer.parseInt(progress[0]));
		}
		@SuppressWarnings("deprecation")
		protected void onPostExecute(String file_url) 
		{
			// dismiss the dialog after the file was downloaded
			if(running == true){
				dismissDialog(progress_bar_type);
				// Displaying downloaded image into image view
				// Reading image path from sdcard
				String imagePath = Environment.getExternalStorageDirectory().toString() + "/AriistoCloud.pdf";
				// setting downloaded into image view
				Log.d(imagePath, "show file");
				File file = new File(Environment.getExternalStorageDirectory(),"AriistoCloud.pdf");
				Uri external = Uri.fromFile(file);
				/*Pdf pdf = new (Pdf);*/
				viewPdf(external);
			}
		}

	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		/*if(pDialog.isShowing()){
			pDialog.setCancelable(true);
		}else{
			pDialog.setCancelable(false);
		}*/
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}



	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode == KeyEvent.KEYCODE_BACK){
			Toast.makeText(context, "ALERT BACK BUTTON", Toast.LENGTH_SHORT).show();
		}

		return super.onKeyDown(keyCode, event);
	}*/

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_pdf, menu);
		return false;
	}
}
