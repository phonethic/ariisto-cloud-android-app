package com.phonethics.ariisto.cloud;

import java.io.File;
import java.util.ArrayList;



import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;

public class Home extends Activity implements OnClickListener {


	ImageView homeImgButton;
	ImageView sevenOneImageButton;
	ImageView porjectImageButton;
	ImageView contactImageButton;
	int imgpos;
	RelativeLayout rel;
	TextView mDotsText[];
	private int mDotsCount;
	private LinearLayout mDotsLayout;
	File externalFile;

	private ArrayList<String> img_Array=new ArrayList<String>();
	MediaPlayer media;
	ImageView projectImageView;
	Bitmap bm;
	Gallery gallery=null;

	LayoutParams lp;

	Animation anim;
	//	TextView  txt;
	TextView txt[];
	int i;
	int counting = 6;

	final String arr1[] = {"FROM" ,"SUPER" ,"PREMIUM","TOWERS" ,"TO", "TOWNSHIPS"};
	final String arr2[] = {"FROM" ,"SUPER" ,"LUXURY","RESIDENCE" ,"TO", "ECONOMICAL\nHOMES"};
	final String arr3[] = {"FROM" ,"BOUTIQUE" ,"OFFICES","TO" ,"LAVISH H.Os", "\n"};
	final String arr4[] = {"DEVELOPING" ,"32 MILLION SQ.FT." ,"18 PROJECTS","1 CITY" ,"MUMBAI",""};


	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		media.pause();
		super.onStop();
	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		media.pause();
		super.onPause();
	}


	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		media.start();
		super.onRestart();
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		media=MediaPlayer.create(getApplicationContext(), R.raw.ariisto_web_music);

		externalFile = new File(Environment.getExternalStorageDirectory(),"CloudProjects.xml");
		
		media.start();
		media.setLooping(true);

		rel=(RelativeLayout)findViewById(R.id.home);

		gallery=(Gallery)findViewById(R.id.gallery);

		img_Array.add("home_1");
		img_Array.add("home_2");
		img_Array.add("home_3");
		img_Array.add("home_4");


		txt = new TextView[counting];


		txt[0] = (TextView) findViewById(R.id.main);
		txt[1] = (TextView) findViewById(R.id.main1);
		txt[2] = (TextView) findViewById(R.id.main2);
		txt[3] = (TextView) findViewById(R.id.main3);
		txt[4] = (TextView) findViewById(R.id.main4);
		txt[5] = (TextView) findViewById(R.id.main5);

		homeImgButton=(ImageView)findViewById(R.id.btn1);
		sevenOneImageButton=(ImageView)findViewById(R.id.btn2);
		porjectImageButton=(ImageView)findViewById(R.id.btn3);
		contactImageButton=(ImageView)findViewById(R.id.btn4);

		homeImgButton.setBackgroundResource(R.drawable.home_act);

		gallery.setAdapter(new GalleryImageAdapter(getApplicationContext(),img_Array));

		gallery.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {

				/* for (int i = 0; i < mDotsCount; i++) {
					 mDotsText[i]
							 .setTextColor(Color.GRAY);
				 }

				 mDotsText[pos]*/
				/*				 .setTextColor(Color.WHITE);*/

				/*				 textAnimation(pos);*/
				if(pos==0)
				{
					textAnim(arr1);
				}
				if(pos==1)
				{
					textAnim(arr2);
				}
				if(pos==2)
				{
					textAnim(arr3);
				}
				if(pos==3)
				{
					textAnim(arr4);
				}


			}//end of onCreate

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		recycleBitmap();
		homeImgButton.setOnClickListener(this);
		sevenOneImageButton.setOnClickListener(this);
		porjectImageButton.setOnClickListener(this);
		contactImageButton.setOnClickListener(this);


	}

	void textAnim(String arr[])
	{
		/*final String arr1[] = {"FROM" ,"SUPER" ,"PREMIUM","TOWERS" ,"TO", "TOWNSHIPS"};*/

		Animation a=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_anim);
		a.setDuration(1000);
		a.reset();

		TextView tx=(TextView)findViewById(R.id.main);
		tx.setText(arr[0]);
		tx.clearAnimation();
		a.setFillAfter(true);
		tx.startAnimation(a);

		a=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_anim);
		a.reset();
		a.setDuration(1000);
		a.setStartOffset(1000);
		tx=(TextView)findViewById(R.id.main1);
		tx.setText(arr[1]);
		tx.clearAnimation();
		a.setFillAfter(true);
		tx.startAnimation(a);

		a=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_anim);
		a.reset();
		a.setDuration(1000);
		a.setStartOffset(2000);
		tx=(TextView)findViewById(R.id.main2);
		tx.setText(arr[2]);
		tx.clearAnimation();
		a.setFillAfter(true);
		tx.startAnimation(a);

		a=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_anim);
		a.reset();
		a.setDuration(1000);
		a.setStartOffset(3000);
		tx=(TextView)findViewById(R.id.main3);
		tx.setText(arr[3]);
		tx.clearAnimation();
		a.setFillAfter(true);
		tx.startAnimation(a);

		a=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_anim);
		a.reset();
		a.setDuration(1000);
		a.setStartOffset(4000);
		tx=(TextView)findViewById(R.id.main4);
		tx.setText(arr[4]);
		tx.clearAnimation();
		a.setFillAfter(true);
		tx.startAnimation(a);

		a=AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_anim);
		a.reset();
		a.setDuration(1000);
		a.setStartOffset(5000);
		tx=(TextView)findViewById(R.id.main5);
		tx.setText(arr[5]);
		tx.clearAnimation();
		a.setFillAfter(true);
		tx.startAnimation(a);


	}

	public void recycleBitmap()
	{
		if(bm!=null)
		{
			bm.recycle();

			bm=null;
		}
	}

	public class GalleryImageAdapter extends BaseAdapter {

		private Context aContext;
		private ArrayList<String> mImageNames=new ArrayList<String>();
		MediaPlayer media;
		ImageView more;

		Handler mHandler=null;
		public GalleryImageAdapter(Context c,ArrayList<String> arr)
		{
			mImageNames.addAll(arr);
			aContext=c;
			/*		media=MediaPlayer.create(aContext, R.raw.photoslidesound);
			mHandler=new Handler();*/


		}



		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mImageNames.size();
		}

		@Override
		public Object getItem(int i) {
			// TODO Auto-generated method stub
			return i;
		}

		@Override
		public long getItemId(int i) {
			// TODO Auto-generated method stub
			return i;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			/*	if(position>0)
			{
				MediaPlayer media=MediaPlayer.create(aContext, R.raw.photoslidesound);
				media.start();
			}	*/	
			/*	if(position>0)
			{
				media.start();
			}*/
			/*more=(ImageView)findViewById(R.id.imgClick);*/
			Log.d("GalleryScrollView", ":"+position);

			if(convertView==null)
			{
				projectImageView=new ImageView(aContext);	

				/*more=new ImageView(aContext);*/

			}
			else
			{
				projectImageView=(ImageView)convertView;

			}

			/*	if(position>0)
			{
				mHandler.post(new MyRunnable());

			}	*/







			projectImageView.setBackgroundColor(Color.BLACK);
			projectImageView.setScaleType(ScaleType.FIT_XY);
			bm=bitmap(mImageNames.get(position));
			projectImageView.setImageBitmap(bm);
			projectImageView.setLayoutParams(new Gallery.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

			return projectImageView;






		}

		private Bitmap bitmap(String name)
		{
			final Bitmap bitmap=BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(name, "drawable", getPackageName()));
			return bitmap;
		}
		public  class MyRunnable implements Runnable {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				/*if(dialog!=null)
				{
					dialog.dismiss();
					dialog=null;
				}*/
				media.start();
			}

		}

	}// end of inner class

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_home, menu);
		return false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if(externalFile.exists())
		{
			externalFile.delete();
		}
		this.finish();
		int pid = android.os.Process.myPid();
		android.os.Process.killProcess(pid);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if(porjectImageButton.getId()==v.getId())
		{

			Intent intent=new Intent(getApplicationContext(),GalleryScrollView.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();

		}
		if(sevenOneImageButton.getId()==v.getId())
		{

			Intent intent=new Intent(getApplicationContext(),Projects.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();

		}
		if(contactImageButton.getId()==v.getId())
		{

			Intent intent=new Intent(getApplicationContext(),ContactUs.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			this.finish();

		}

	}


}
