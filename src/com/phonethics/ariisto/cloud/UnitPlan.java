package com.phonethics.ariisto.cloud;



import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.ToggleButton;

public class UnitPlan extends Activity implements OnClickListener, OnTouchListener {

	private ToggleButton 	tog_button;
	ImageView 				imgUnitPlan;
	boolean					thisImage = true;
	Bitmap 					bm;
	TouchImageView 			unitPlanImg; 
	TouchImageViewUnit 		unitPlanImgBack;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_unit_plan);

		tog_button 		= (ToggleButton) findViewById(R.id.button1);
		tog_button.setOnClickListener(this);

		unitPlanImg = (TouchImageView) findViewById(R.id.image_unit_plan);
		unitPlanImg.setImageResource(R.drawable.bhk4_front);
		unitPlanImg.setMaxZoom(4f);
		
		unitPlanImgBack = (TouchImageViewUnit) findViewById(R.id.image_unit_plan_back);
		unitPlanImgBack.setMaxZoom(4f);
	
		unitPlanImg.setVisibility(View.VISIBLE);
		unitPlanImgBack.setVisibility(View.GONE);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_unit_plan, menu);
		return false;
	}

	@Override
	public void onClick(View v) {
		if(tog_button.isChecked() ){
		/*
			unitPlanImg.setImageResource(R.drawable.bhk4_back);
			unitPlanImg.setMaxZoom(4f);
			thisImage = true;
		*/	
			
			
			unitPlanImg.setVisibility(View.GONE);
			unitPlanImgBack.setVisibility(View.VISIBLE);
			
		}else{
		/*	
			unitPlanImg.setImageResource(R.drawable.bhk4_front);
			unitPlanImg.setMaxZoom(4f);
			thisImage = false;*/
	
			
			unitPlanImg.setVisibility(View.VISIBLE);
			unitPlanImgBack.setVisibility(View.GONE);
		}

	


	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}


}
