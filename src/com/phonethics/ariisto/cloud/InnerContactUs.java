package com.phonethics.ariisto.cloud;


import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class InnerContactUs extends Activity implements OnClickListener {

	ImageView txt_web,txt_address;
	ImageView txt_email;
	ImageView img_arristo_phone;
	Context context;

	ImageView homeImgButton;
	ImageView sevenOneImageButton;
	ImageView porjectImageButton;
	ImageView contactImageButton;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inner_contact_us);

		context=this;
		txt_web=(ImageView)findViewById(R.id.ariistoWeb);
		txt_email=(ImageView)findViewById(R.id.ariistoEmail);
		img_arristo_phone=(ImageView)findViewById(R.id.ariistoPhone);
		txt_address = (ImageView)findViewById(R.id.arristoCloudAddress);

		/*homeImgButton=(ImageView)findViewById(R.id.btn1);
		sevenOneImageButton=(ImageView)findViewById(R.id.btn2);
		porjectImageButton=(ImageView)findViewById(R.id.btn3);
		contactImageButton=(ImageView)findViewById(R.id.btn4);

		contactImageButton.setImageResource(R.drawable.contact_act);
		 */
		/*homeImgButton.setOnClickListener(this);
		sevenOneImageButton.setOnClickListener(this);
		porjectImageButton.setOnClickListener(this);
		contactImageButton.setOnClickListener(this);*/

		txt_web.setOnClickListener(this);
		txt_email.setOnClickListener(this);
		img_arristo_phone.setOnClickListener(this);
		txt_address.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.ariistoWeb)
		{
			Intent web=new Intent(Intent.ACTION_VIEW);
			web.setData(Uri.parse(getResources().getString(R.string.webUrl)));
			startActivity(web);
		}
		else if(v.getId()==R.id.ariistoEmail)
		{
			Intent email=new Intent(Intent.ACTION_SEND);
			email.setType("plain/text");
			/*email.putExtra(android.content.Intent.EXTRA_EMAIL,getResources().getString(R.string.emailId));*/
			email.putExtra("android.intent.extra.EMAIL",new String[]{getResources().getString(R.string.emailId)});
			email.putExtra("android.intent.extra.SUBJECT", "Subject");
			email.putExtra("android.intent.extra.TEXT", "Enter your text here");
			email.setType("text/plain");
			startActivity(Intent.createChooser(email, "Send mail..."));
		}
		else if(v.getId()==R.id.ariistoPhone)
		{
			TelephonyManager tm= (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
			if(tm.getPhoneType()==TelephonyManager.PHONE_TYPE_NONE){
				//No calling functionality
			}
			else
			{
				AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
				alertDialog.setIcon(R.drawable.ariistologo);
				alertDialog.setTitle(" ");
				alertDialog.setMessage("Do you want to call on this Number?");
				alertDialog.setCancelable(true);
				alertDialog.setPositiveButton("Yes",new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent call = new Intent(android.content.Intent.ACTION_DIAL);
						call.setData(Uri.parse(getResources().getString(R.string.contactNo)));
						startActivity(call);
					}
				});	
				alertDialog.setNegativeButton("No",new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});

				AlertDialog alert=alertDialog.show();
			}

		}else if(v.getId() == R.id.arristoCloudAddress){
			Intent intent = new Intent(context, GoogleMap.class);
			startActivity(intent);
			overridePendingTransition(R.anim.lefttorightslide,R.anim.righttoleftslide);
		}

		/*if(homeImgButton.getId()==v.getId())
   		{

   			Intent intent=new Intent(getApplicationContext(),Home.class);
   			startActivity(intent);
   			overridePendingTransition(0, 0);
   			this.finish();

   		}
   		if(sevenOneImageButton.getId()==v.getId())
   		{

   			Intent intent=new Intent(getApplicationContext(),Projects.class);
   			startActivity(intent);
   			overridePendingTransition(0, 0);
   			this.finish();

   		}
   		if(porjectImageButton.getId()==v.getId())
   		{

   			Intent intent=new Intent(getApplicationContext(),GallerScrollView.class);
   			startActivity(intent);
   			overridePendingTransition(0, 0);
   			this.finish();

   		}*/

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_contact_us, menu);
		return false;
	}
}


