package com.phonethics.ariisto.cloud;

import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.phonethics.ariisto.cloud.GoogleMap.MapOverlay;

public class AddressGoogleMap extends MapActivity {

	MapView mapView;
	MapController mc;
	GeoPoint p;
	Bitmap bmp;
	ImageView imgv_pin;
	ImageView imgv_header;
	RelativeLayout imgv_head;

	class MapOverlay extends com.google.android.maps.Overlay implements OnClickListener
	{
		@Override
		public boolean draw(Canvas canvas, MapView mapView,
				boolean shadow, long when)
		{
			super.draw(canvas, mapView, shadow);

			//---translate the GeoPoint to screen pixels---

			Point screenPts = new Point();
			mapView.getProjection().toPixels(p, screenPts);

			//---add the marker---

			bmp = BitmapFactory.decodeResource(getResources(), R.drawable.pinimage);
			/*canvas.drawBitmap(bmp, screenPts.x, screenPts.y-50, null);*/
			imgv_pin.setImageBitmap(bmp);
			RelativeLayout.LayoutParams params=(RelativeLayout.LayoutParams)imgv_pin.getLayoutParams();
			params.setMargins(screenPts.x, screenPts.y-50, 0, 0);
			imgv_pin.setLayoutParams(params);
			imgv_pin.setOnClickListener(this);
			return true;
		}

		@Override
		public boolean onTouchEvent(MotionEvent event, MapView view){

			if (event.getAction() == 1) {

				GeoPoint p = mapView.getProjection().fromPixels(
						(int) event.getX(),
						(int) event.getY());

			}
			return false;

		}
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Toast.makeText(getBaseContext(),"Location: "+  "Main Office" , Toast.LENGTH_SHORT).show();
		}
	}
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_google_map);
        
        imgv_header=(ImageView)findViewById(R.id.imgaview_address_googleMap);

		imgv_head=(RelativeLayout)findViewById(R.id.header);

		mapView = (MapView) findViewById(R.id.mapView);
		imgv_pin=(ImageView)findViewById(R.id.pin);
		mc = mapView.getController();
		String coordinates[] = {"19.11545214054174", "72.85106802380369"};
		double lat = Double.parseDouble(coordinates[0]);
		double lng = Double.parseDouble(coordinates[1]);
		p = new GeoPoint(
				(int) (lat * 1E6),
				(int) (lng * 1E6));
		mc.animateTo(p);
		mc.setZoom(17);


		MapOverlay mapOverlay = new MapOverlay();					//to show pin
		List<Overlay> listOfOverlays = mapView.getOverlays();
		listOfOverlays.clear();

		listOfOverlays.add(mapOverlay);

		Animation animate = AnimationUtils.loadAnimation(this, R.anim.pinanim);

		animate.setStartOffset(800);
		imgv_pin.startAnimation(animate);

		animate.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				imgv_head.bringToFront();
				imgv_header.bringToFront();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub

			}
		});

		imgv_pin.setAnimation(animate);
		animate.setFillAfter(true);
		/*	Animation an = AnimationUtils.loadAnimation(this,R.anim.pinanim);
		imgv_pin.startAnimation(an);*/

		mapView.invalidate();
	}
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_address_google_map, menu);
        return false;
    }

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	 @Override
		public void onBackPressed() {
			// TODO Auto-generated method stub
			
	    	super.onBackPressed();
			overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
			this.finish();
		}
}
