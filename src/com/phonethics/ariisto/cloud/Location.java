package com.phonethics.ariisto.cloud;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class Location extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        
       /* ImageView img = (ImageView) findViewById(R.id.imageView_location_down_button);
        img.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent=new Intent(getApplicationContext(),Control_tools.class);
				startActivity(intent);				
			}
		});*/
        
        /*ImageView locationImage=(ImageView)findViewById(R.id.imageLocation);
        locationImage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			
				int id=getResources().getIdentifier(getResources().getResourceEntryName(R.drawable.location_map), "drawable", getPackageName());
				int id=R.drawable.locationmap;
				Toast.makeText(getApplicationContext(), "Id is : "+id, Toast.LENGTH_LONG).show();
				Intent intent=new Intent(getApplicationContext(),PinchToZoom.class);
				intent.putExtra("Image_id",id);
				startActivity(intent);
				Bitmap bm=BitmapFactory.decodeResource(getResources(), R.drawable.locationmap);
				SandboxView1 img=new SandboxView1(getApplicationContext());
				img.setImageBitmap(bm);
				img.setMaxZoom(5);
				setContentView(img);
			}
		});*/
        
        TouchImageView locationImg = (TouchImageView) findViewById(R.id.image_location);
        locationImg.setMaxZoom(3f);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_location, menu);
        return false;
    }

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		overridePendingTransition(R.anim.slide_out_left,R.anim.slide_out_right);
		this.finish();
	}
    
}
