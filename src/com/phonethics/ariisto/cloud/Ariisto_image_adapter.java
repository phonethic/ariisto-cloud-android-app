package com.phonethics.ariisto.cloud;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;



public class Ariisto_image_adapter extends BaseAdapter {
	private Context context=null;
	private ArrayList<String> control_img=new ArrayList<String>();
	
	public Ariisto_image_adapter(Context context,ArrayList<String> control_img1) {
		control_img.addAll(control_img1);
		this.context = context;
		
	}

	@Override
	public int getCount() {
		
		return control_img.size();
		
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View grid;
		if(convertView == null){
			
			grid = new View(context);
			
			grid = inflator.inflate(R.layout.grid_image_layout,null);
			
			
			}
		else{
			grid = (View) convertView;
		}
		ImageView img = (ImageView) grid.findViewById(R.id.image_insidegrid);
		img.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/"+control_img.get(position),"drawable",context.getPackageName())));
		img.setScaleType(ScaleType.FIT_XY);
		return grid;
	}
	
		
	
}
